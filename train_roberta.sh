#!/usr/bin/env bash
#OAR -n train_without_ents
#OAR -O /udd/huthomas/taln_experiments/without_ents.out
#OAR -E /udd/huthomas/taln_experiments/without_ents.err

#patch to be aware of "module" inside a job
. /etc/profile.d/modules.sh
module load spack/cuda/11.3.1
module load spack/cudnn/8.0.4.30-11.0-linux-x64

# conda deactivate
source /srv/longdd/huthomas/venv/bin/activate

EXEC_FILE=/udd/huthomas/taln_experiments/main.py

echo
echo =============== RUN ${OAR_JOB_ID} ===============
echo Run $EXEC_FILE at `date +"%T, %d-%m-%Y"`


export DATASET="/udd/huthomas/relation_encoder/dataset"
export MODELS="/srv/tempdd/huthomas/models"
export FEATURES="/srv/tempdd/huthomas/features"
export CHECKPOINTS="/srv/tempdd/huthomas/models/triplet_encoder/checkpoints"
export TOKENIZERS_PARALLELISM=true
export PYTHONUSERBASE="/srv/tempdd/huthomas/.local"
export TMPDIR="/srv/tempdd/huthomas/tmp"


python3 $EXEC_FILE train --train_tokens train.json --val_tokens dev.json --test_tokens test.json --embedding_model distilroberta-base --model_type without_ents --shortest_path False --ent_representation ent --nb_epochs 128 --batch_size 16 --period 0.5