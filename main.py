import click
import pandas as pd
import numpy as np
from sklearn.metrics import f1_score, accuracy_score, recall_score, classification_report
from sklearn.neighbors import KNeighborsClassifier, KNeighborsTransformer, NearestCentroid
from sklearn.model_selection import GridSearchCV
from scipy.spatial import distance
from scipy.stats import spearmanr

import pytorch_lightning as pl
from pytorch_lightning.loggers import TensorBoardLogger
from pytorch_lightning.callbacks import ModelCheckpoint
from pytorch_lightning.callbacks.early_stopping import EarlyStopping

from os import getenv, listdir, environ
import sys
import json
import pickle
import itertools as it

from gensim.models import KeyedVectors
import stanza

from rich.progress import track
from libraries.log import logger
from libraries.strategies import *
from data_holder import RelationDataset, TokensDataset
from datasets import Dataset
from torch.utils.data import DataLoader, TensorDataset
from modelization import RelationPredictorWithEnts, RelationPredictor, RelationEncoderProxy, CNN_classifier, TFIDFPredictor, BiLSTM, LUKERelationPredictor

from transformers import TrainingArguments, Trainer, AutoTokenizer, AutoModelForSequenceClassification, AutoModel, RobertaConfig, RobertaAdapterModel
from sentence_transformers import SentenceTransformer
#import matplotlib.pylab as plt
#import seaborn as sns

@click.group(chain=False, invoke_without_command=True)
@click.pass_context
def router(ctx):
    ctx.ensure_object(dict)
    logger.debug('nltk initialization ...!')

    path2models = getenv('MODELS')
    path2dataset = getenv('DATASET')
    path2features = getenv('FEATURES')
    path2nltk_data = getenv('NLTK_DATA')
    path2checkpoints = getenv('CHECKPOINTS')
    
    logger.debug('env variables loading')

    ctx.obj['path2models'] = path2models
    ctx.obj['path2dataset'] = path2dataset 
    ctx.obj['path2features'] = path2features  
    ctx.obj['path2nltk_data'] = path2nltk_data 
    ctx.obj['path2checkpoints'] = path2checkpoints

    logger.debug('nltk initialization')
    nltk.download('punkt', download_dir=path2nltk_data) 
    nltk.download('stopwords', download_dir=path2nltk_data)
    
    ctx.ensure_object(dict)
    subcommand = ctx.invoked_subcommand 
    if subcommand is None:
        logger.debug('use --help option to see availables options')
    else:
        logger.debug(f'{subcommand} was called')

@router.command()
@click.option('--embeddings_filename', help='corpus file name', type=str)
@click.option('--batch_size', help="Size of embedding batches for faster computation", type=int, default=500)
@click.option('--sim_threshold', help="Threshold for determining whether 2 embeddings are similar", type=float, default=10.0)
@click.pass_context
def evaluate_embeddings(ctx, embeddings_filename, batch_size, sim_threshold):
    """Evaluate the quality of relation embeddings through a similarity prediction task..."""
    try:
        device = th.device('cuda:0' if th.cuda.is_available() else 'cpu')

        path2models = ctx.obj['path2models']
        path2dataset = ctx.obj['path2dataset']
        path2features = ctx.obj['path2features']

        path2embeddings = path.join(path2features, embeddings_filename)

        logger.debug(f"Opening embedding file {path2embeddings}...")
        relation_embeddings = deserialize(path2embeddings, pickle)
        logger.success("Succesfully loaded files !")

        logger.debug("Beginning embeddings evaluation...")
        dataset = TensorDataset(th.tensor([int(l) for l in relation_embeddings['labels']]), relation_embeddings['embeddings'])
        dataloader = DataLoader(dataset, batch_size)
        with th.no_grad():
            y_pred = th.tensor([])
            y_true = th.tensor([])
            for labels, embeddings in track(iter(dataloader),"Evaluating batches..."):
                sim = embeddings @ embeddings.T
                truth = labels.repeat((labels.shape[0],1)) - labels.unsqueeze(-1).repeat((1,labels.shape[0]))
                truth = truth.bool().logical_not().float()
                flat_labels = truth.flatten()
                flat_sim = (sim>sim_threshold).float().flatten()
                y_pred = th.concat([y_pred, flat_sim], dim=0)
                y_true = th.concat([y_true, flat_labels], dim=0)
        logger.success("Successfuly computed embeddings similarity !")
        logger.debug("Evaluating computed similarities...")
        metrics = {'f1':f1_score(y_true, y_pred),'accuracy':accuracy_score(y_true,y_pred), 'recall':recall_score(y_true,y_pred)}
        embeddings = embeddings.to('cpu')
        logger.success("Successfuly evaluated embeddings !")
        logger.info(f"F1 : {metrics['f1']} | Accuracy : {metrics['accuracy']} | Recall : {metrics['recall']}")
        ## documents = corpus['text'].tolist()
        
    except Exception as e:
        logger.error(e)


@router.command()
@click.option('--train_tokens', type=str, help="file path for training relation tokens")
@click.option('--val_tokens', type=str, help="file path for validation relation tokens")
@click.option('--test_tokens', type=str, help="file path for test relation tokens")
@click.option('--embedding_model', type=str, help="encoder model to fine-tune for relation/description matching")
@click.option('--model_type', type=str, help="Type of model to train (without_ents, with_ents, with_ents_proxy, tfidf, cnn, luke)")
@click.option('--shortest_path', type=bool, help ="Whether to keep the whole relation or just the shortest path between the entities")
@click.option('--ent_representation', type=str, help="Way of representing entities in text (ent, ent_type, ent_markup)")
@click.option('--nb_epochs', type=int, default=32)
@click.option('--batch_size', type=int, default=16)
@click.option('--checkpoint', type=str, default='')
@click.option('--period', default=1.0, type=float)
@click.option('--no_relation',default=False, type = bool)
@click.option('--dataset',default="TACRED", type = str)
@click.option('--shuffle_words',default=False, type = bool)
@click.pass_context
def train(ctx, train_tokens, val_tokens, test_tokens, embedding_model, model_type, shortest_path, ent_representation, nb_epochs, batch_size, \
          checkpoint, period, no_relation, dataset, shuffle_words):
    """Train the Relation prediction model with entities types"""
    device = th.device('cuda:0' if th.cuda.is_available() else 'cpu')
    logger.debug(f'target device : {device}')

    path2checkpoints = ctx.obj['path2checkpoints']
    path2dataset = ctx.obj['path2dataset']
    path2features = ctx.obj['path2features']

    path2train = path.join(path2dataset, train_tokens)
    path2val = path.join(path2dataset, val_tokens)
    path2test = path.join(path2dataset, test_tokens)
    path2weights = path.join(path2checkpoints, checkpoint)

    logger.debug("Loading tokenizer...")
    tokenizer = AutoTokenizer.from_pretrained(embedding_model)
    logger.success(f"Succesfully loaded tokenizer from {embedding_model}")
    logger.debug("Beginning loading datasets and dataloaders...")

    train_dataset = EntityDataset(path2train, shortest_path=shortest_path, ent_representation=ent_representation, keep_no_rel=no_relation, dataset=dataset, shuffle_words=shuffle_words)#, savepath='dataset/tacred_train.pkl')
    val_dataset = EntityDataset(path2val, shortest_path=shortest_path, ent_representation=ent_representation, keep_no_rel=no_relation, dataset=dataset, shuffle_words=shuffle_words)#, savepath='dataset/tacred_dev.pkl')
    test_dataset = EntityDataset(path2test, shortest_path=shortest_path, ent_representation=ent_representation, keep_no_rel=no_relation, dataset=dataset, shuffle_words=shuffle_words)#, savepath='dataset/tacred_test.pkl')


    train_dataloader = DataLoader(train_dataset, batch_size, shuffle=True, num_workers=8, collate_fn=collate_fn)
    val_dataloader = DataLoader(val_dataset, batch_size, num_workers=8, collate_fn=collate_fn)
    test_dataloader = DataLoader(test_dataset, batch_size, num_workers=8, collate_fn=collate_fn)
    logger.debug('dataset and dataloader were initialized')

    id2rel = {v:k for k,v in train_dataset.label2id.items()}
    id2ents = {v:k for k,v in train_dataset.ent2id.items()}
    model_type_dict = {
        "with_ents":[['relation_predictor_adapter'], ['relation_predictor', 'head_ent_predictor', 'tail_ent_predictor'], [id2rel, id2ents, id2ents], RelationPredictorWithEnts, {'id2ents':id2ents, 'id2rel':id2rel}],
        "without_ents":[['relation_predictor_adapter'], ['relation_predictior_adapter'], [id2rel], RelationPredictor, {}],
        "luke":[[], [], [], LUKERelationPredictor, {'label2id':train_dataset.label2id}],
        "with_ents_proxy":[['relation_predictor_adapter'], ['head_ent_predictor', 'tail_ent_predictor'], [id2ents, id2ents], RelationEncoderProxy, {'label2id':train_dataset.label2id}],
        "cnn":[[], [], [], CNN_classifier, {'kernel_num': 128,'kernel_filters': (3,4,5),'embed_dim': 768,'dropout': 0.1, 'id2label':id2rel}],
        "tfidf":[[], [], [], TFIDFPredictor, {'train_examples':train_dataset.sentence, 'train_ent1':train_dataset.ent1, 'train_ent2':train_dataset.ent2, \
            'type2id':train_dataset.ent2id, 'label2id':train_dataset.label2id}],
        "lstm":[[], [], [], BiLSTM, {'label2id':train_dataset.label2id}]
    }
    adapters, heads, id2labels, model_class, kwargs = model_type_dict[model_type]
    
    for version in range(5):
        logger.debug("Loading encoder...")
        tokenizer, encoder = build_model(model_name=embedding_model, adapters=adapters, num_labels=len(id2rel), classification_heads=heads, headid2label=id2labels)
        special_tokens_dict = {'additional_special_tokens': [train_dataset.ent_start,train_dataset.ent_end]}
        num_added_toks = tokenizer.add_special_tokens(special_tokens_dict)
        encoder.resize_token_embeddings(len(tokenizer))

        logger.success(f"Succesfully loaded encoder from {embedding_model}")

    
        th.manual_seed(version)
        
        environ['TOKENIZERS_PARALLELISM']='true'
        name = model_type+'_'+embedding_model.replace('/','-')+'_shortest'+str(shortest_path)+'_'+ent_representation+'_norel'+str(no_relation)+'_'+str(dataset)+'_shuffle'+str(shuffle_words)
        tb_logger = pl.loggers.TensorBoardLogger(save_dir=path2checkpoints, name=name)
        callback = ModelCheckpoint(dirpath=tb_logger.log_dir, monitor="val_f1", save_top_k=3, mode='max')
        es_callback = EarlyStopping(monitor="val_f1", mode="max", patience=5)
        trainer = pl.Trainer(accelerator='gpu', callbacks=[callback, es_callback], devices=1, max_epochs=nb_epochs, logger=tb_logger, val_check_interval=period)
        
        model = model_class(tokenizer=tokenizer, encoder=encoder, embedding_dim=768, \
            relation_types=len(train_dataset.label2id), entity_types=len(train_dataset.ent2id), batch_size=batch_size, **kwargs)

        print(model)
        logger.debug('model was initialized')

        logger.debug('training will start in 1 s')

        trainer.fit(model, train_dataloader, val_dataloader)
        
        if model_type in ['with_ents_proxy']:
            logger.debug("Proxy ready for second step in training")
            model.encoder_disable_grad()
            callback = ModelCheckpoint(dirpath=tb_logger.log_dir, monitor="val_f1_relation", save_top_k=3, mode='max')
            es_callback = EarlyStopping(monitor="val_f1_relation", mode="max", patience=3)
            trainer = pl.Trainer(accelerator='gpu', callbacks=[callback, es_callback], devices=1, max_epochs=nb_epochs, logger=tb_logger, val_check_interval=period)
            trainer.fit(model,train_dataloader, val_dataloader)
        print(callback.best_model_path)
        model = model_class.load_from_checkpoint(callback.best_model_path, tokenizer=tokenizer, encoder=encoder, embedding_dim=768, \
            relation_types=len(train_dataset.label2id), entity_types=len(train_dataset.ent2id), batch_size=batch_size, **kwargs)
        model.to(device)
        model.eval()
        predictions = trainer.predict(model, test_dataloader)

        predictions = [p.argmax(-1) for p in predictions]

        predictions = th.concat(predictions).tolist()
        predictions = [id2rel[r] for r in predictions]
        targets = test_dataset.relation
        print(classification_report(targets, predictions, zero_division=0.0))
        metrics = classification_report(targets, predictions, output_dict=True, zero_division=0.0)

        path2metrics = "/srv/tempdd/huthomas/metrics/"+name+str(version)+".pkl"
        with open(path2metrics, 'wb') as f:
            pickle.dump([metrics, targets, predictions, test_dataset.idx], f)

    logger.success(f'training ended succesfully')

@router.command()
@click.option('--val_tokens', type=str, help="file path for validation relation tokens")
@click.option('--test_tokens', type=str, help="file path for test relation tokens")
@click.option('--embedding_model', type=str, help="encoder model to fine-tune for relation/description matching")
@click.option('--model_type', type=str, help="Type of model to train (without_ents, with_ents, with_ents_proxy, tfidf, cnn)")
@click.option('--shortest_path', type=bool, help ="Whether to keep the whole relation or just the shortest path between the entities")
@click.option('--ent_representation', type=str, help="Way of representing entities in text (ent, ent_type, ent_markup)")
@click.option('--batch_size', type=int, default=16)
@click.option('--checkpoint', type=str, default='')
@click.pass_context
def evaluate_proxy(ctx, val_tokens, test_tokens, embedding_model, model_type, shortest_path, ent_representation, batch_size, checkpoint):
    
    device = th.device('cuda:0' if th.cuda.is_available() else 'cpu')
    logger.debug(f'target device : {device}')

    path2features = "/srv/tempdd/huthomas/features"
    path2checkpoints = "/srv/tempdd/huthomas/models/taln_results/"
    path2dataset = "/udd/huthomas/relation_encoder/dataset/tacred/data/json/"

    path2val = path.join(path2dataset, val_tokens)
    path2test = path.join(path2dataset, test_tokens)
    path2weights = path.join(path2checkpoints, checkpoint)

    logger.debug("Loading tokenizer...")
    tokenizer = AutoTokenizer.from_pretrained(embedding_model)
    logger.success(f"Succesfully loaded tokenizer from {embedding_model}")
    logger.debug("Beginning loading datasets and dataloaders...")

    val_dataset = EntityDataset(path2val, shortest_path=shortest_path, ent_representation=ent_representation)#, savepath='dataset/tacred_dev.pkl')
    test_dataset = EntityDataset(path2test, shortest_path=shortest_path, ent_representation=ent_representation)#, savepath='dataset/tacred_test.pkl')

    val_dataloader = DataLoader(val_dataset, batch_size, num_workers=16, collate_fn=collate_fn)
    test_dataloader = DataLoader(test_dataset, batch_size, num_workers=16, collate_fn=collate_fn)
    logger.debug('dataset and dataloader were initialized')

    id2rel = {v:k for k,v in val_dataset.label2id.items()}
    id2ents = {v:k for k,v in val_dataset.ent2id.items()}
    
    adapters, heads, id2labels, model_class, kwargs = ['relation_predictor_adapter'], ['head_ent_predictor', 'tail_ent_predictor'], [id2ents, id2ents], RelationEncoderProxy, {}
    
    logger.debug("Loading encoder...")
    tokenizer, encoder = build_model(model_name=embedding_model, adapters=adapters, num_labels=len(id2rel), classification_heads=heads, headid2label=id2labels)
    special_tokens_dict = {'additional_special_tokens': [val_dataset.ent_start,val_dataset.ent_end]}
    num_added_toks = tokenizer.add_special_tokens(special_tokens_dict)
    encoder.resize_token_embeddings(len(tokenizer))

    logger.success(f"Succesfully loaded encoder from {embedding_model}")
    
    environ['TOKENIZERS_PARALLELISM']='true'
    name = model_type+'_'+embedding_model+'_shortest='+str(shortest_path)+'_'+ent_representation
    trainer = pl.Trainer(accelerator='gpu', devices=1)
    
    model = model_class.load_from_checkpoint(path2weights, tokenizer=tokenizer, encoder=encoder, embedding_dim=768, \
        relation_types=len(val_dataset.label2id), entity_types=len(val_dataset.ent2id), batch_size=batch_size, **kwargs)

    print(model)
    logger.debug('model was initialized')

    model.to(device)
    model.eval()
    embeddings = trainer.predict(model, val_dataloader)
    embeddings = th.concat(embeddings).cpu().numpy()
    targets = val_dataset.relation
    parameters = {'n_neighbors':list(range(1,10))}
    model = KNeighborsClassifier()
    clf = GridSearchCV(model, parameters, scoring='f1_micro')
    clf.fit(embeddings, targets)

    embeddings = trainer.predict(model, test_dataloader)
    embeddings = th.concat(embeddings).cpu().numpy()
    targets = test_dataset.relation
    predictions = clf.predict(embeddings)

    print(classification_report(targets, predictions, zero_division=0.0))
    metrics = classification_report(targets, predictions, output_dict=True, zero_division=0.0)

    path2metrics = "/srv/tempdd/huthomas/metrics/"+name+".pkl"
    with open(path2metrics, 'wb') as f:
        pickle.dump([metrics, targets, predictions, test_dataset.idx], f)

@router.command()
@click.option('--fewrel_path', type=str, help="file path for FewRel")
@click.option('--out_path', type=str, help="file path for FewRel")
@click.pass_context
def fewrel_to_tacred_format(ctx, fewrel_path, out_path):

    nlp = stanza.Pipeline(lang='en', models_dir="/srv/tempdd/huthomas/stanfordnlp_resources/", use_gpu=True, tokenize_pretokenized=True)
    def word_to_char(begin, end, tokens):
        return sum([len(tok) for tok in tokens[:begin]])+begin, sum([len(tok) for tok in tokens[:end+1]])+end
    
    def in_bounds(inlower, inupper, outlower, outupper):
        return (inlower>=outlower or inupper<=outupper) and inlower<outupper and inupper>outlower
    
    with open(fewrel_path, 'r') as f:
        data = json.load(f)
        
    examples = []
    for labelkey,labellist in data.items():
        for example in labellist:
            example['relation'] = labelkey
        examples+=labellist
    
    dataset = []
    for idx, example in enumerate(track(examples)):
        doc = nlp([example['tokens']])
        sstart, send = example['h'][-1][0][0], example['h'][-1][0][-1]
        slower, supper = word_to_char(sstart, send, example['tokens'])
        ostart, oend = example['t'][-1][0][0], example['t'][-1][0][-1]
        olower, oupper = word_to_char(ostart, oend, example['tokens'])
        out = {'id':idx, 'relation':example['relation'], 'token':doc.get('text'), 'subj_start':sstart, 'subj_end':send, \
               'obj_start':ostart, 'obj_end':oend}
        
        out['stanford_head'] = doc.get('head')
        entlist = doc.build_ents()
        hname, tname = example['h'][0], example['t'][0]
        htype, ttype = '', ''

        sscore, oscore = 10000, 10000
        for ent in entlist:
            if in_bounds(slower, supper, ent.start_char, ent.end_char):
                
                if sscore>(abs(ent.start_char-slower)+abs(ent.end_char-supper)):
                    htype = ent._type
                    sscore = (abs(ent.start_char-slower)+abs(ent.end_char-supper))
                    
            if in_bounds(olower, oupper, ent.start_char, ent.end_char):
                
                if oscore>(abs(ent.start_char-olower)+abs(ent.end_char-oupper)):
                    ttype = ent._type
                    oscore = (abs(ent.start_char-olower)+abs(ent.end_char-oupper))
                    
        htype = htype if htype else 'OTHER'
        ttype = ttype if ttype else 'OTHER'
        out['subj_type']=htype
        out['obj_type']=ttype
        dataset.append(out)
    with open(out_path, 'wb') as f:
        pickle.dump(dataset, f)


@router.command()
@click.option('--dataset_path', type=str, help="file path of json dataset")
@click.option('--out_path', type=str, help="pickle file to save triples and dictionaries")
@click.option('--keyed_vectors', type=str, help="pickle file path for pretrained keyed vectors")
@click.option('--from_checkpoint', type=str, default="", help="pickle file path for non-empty dictionnaries to start from")
@click.pass_context
def build_triples(ctx, dataset_path, out_path, keyed_vectors, from_checkpoint):
    logger.debug("Loading Keyed Vectors (this might take a while)...")
    with open(keyed_vectors, 'rb') as f:
        kv = pickle.load(f)
    logger.success(f"{len(kv)} Keyed Vectors were loaded !")
    
    logger.debug(f"Loading dataset...")
    with open(dataset_path, 'r') as f:
        dataset = json.load(f)
    logger.success(f"Successfully loaded dataset of {len(dataset)} examples !")
    INDEX_TO_KEY = kv.index_to_key
    entity2id = {}
    relation2id = {}

    embedding_size = kv.vector_size
    id2vector = np.array([]).reshape(0,embedding_size)
    triples = np.array([], dtype=np.int32).reshape(0,3)

    if from_checkpoint:
        with open(from_checkpoint, 'rb') as f:
            _, entity2id, relation2id, id2vector = pickle.load(f)

    for example in track(dataset, "Processing triples..."):
        head = '_'.join(example['token'][example['subj_start']:example['subj_end']+1])
        tail = '_'.join(example['token'][example['obj_start']:example['obj_end']+1])
        relation = example['relation']
        if not head in entity2id:
            entity2id[head]=len(entity2id)
            vector = np.expand_dims(get_vector(head, kv, INDEX_TO_KEY),axis=0)
            id2vector = np.concatenate([id2vector, vector], axis=0)
        if not tail in entity2id:
            entity2id[tail]=len(entity2id)
            vector = np.expand_dims(get_vector(tail, kv, INDEX_TO_KEY),axis=0)
            id2vector = np.concatenate([id2vector, vector], axis=0)
        if not relation in relation2id and relation!='no_relation':
            relation2id[relation]=len(relation2id)
        if relation != 'no_relation':
            triples = np.concatenate([triples, np.array([[entity2id[head], relation2id[relation], entity2id[tail]]])], axis=0)
    logger.success("Succesfully built triples!")
    with open(out_path, 'wb') as f:
        pickle.dump([triples, entity2id, relation2id, id2vector], f)
    logger.success(f"Triples were saved at {out_path} !")

@router.command()
@click.option('--train_tokens', type=str, help="file path for training relation tokens")
@click.option('--val_tokens', type=str, help="file path for validation relation tokens")
@click.option('--test_tokens', type=str, help="file path for test relation tokens")
@click.option('--embedding_model', type=str, help="encoder model to fine-tune for relation/description matching")
@click.option('--model_type', type=str, help="Type of model to train (without_ents, with_ents, with_ents_proxy, tfidf, cnn)")
@click.option('--shortest_path', type=bool, help ="Whether to keep the whole relation or just the shortest path between the entities")
@click.option('--ent_representation', type=str, help="Way of representing entities in text (ent, ent_type, ent_markup)")
@click.option('--batch_size', type=int, default=16)
@click.option('--checkpoint', type=str, default='')
@click.option('--no_relation',default=False, type = bool)
@click.option('--dataset',default="TACRED", type = str)
@click.option('--shuffle_words',default=False, type = bool)
@click.option('--version',default=0, type = int)
@click.option('--n_samples',default=50, type = int)
@click.option('--n_neighbors',default=3, type = int)
@click.pass_context
def embed_and_analyse(ctx, train_tokens, val_tokens, test_tokens, embedding_model, model_type, shortest_path, ent_representation, batch_size, \
          checkpoint, no_relation, dataset, shuffle_words, version, n_samples, n_neighbors):
    """Train the Relation prediction model with entities types"""
    device = th.device('cuda:0' if th.cuda.is_available() else 'cpu')
    logger.debug(f'target device : {device}')

    
    path2checkpoints = ctx.obj['path2checkpoints']
    path2dataset = ctx.obj['path2dataset']
    path2features = ctx.obj['path2features']

    path2train = path.join(path2dataset, train_tokens)
    path2val = path.join(path2dataset, val_tokens)
    path2test = path.join(path2dataset, test_tokens)
    path2weights = path.join(path2checkpoints, checkpoint)


    logger.debug("Loading tokenizer...")
    tokenizer = AutoTokenizer.from_pretrained(embedding_model)
    logger.success(f"Succesfully loaded tokenizer from {embedding_model}")
    logger.debug("Beginning loading datasets and dataloaders...")

    train_dataset = EntityDataset(path2train, shortest_path=shortest_path, ent_representation=ent_representation, keep_no_rel=no_relation, dataset=dataset, shuffle_words=shuffle_words)#, savepath='dataset/tacred_train.pkl')
    val_dataset = EntityDataset(path2val, shortest_path=shortest_path, ent_representation=ent_representation, keep_no_rel=no_relation, dataset=dataset, shuffle_words=shuffle_words)#, savepath='dataset/tacred_dev.pkl')
    test_dataset = EntityDataset(path2test, shortest_path=shortest_path, ent_representation=ent_representation, keep_no_rel=no_relation, dataset=dataset, shuffle_words=shuffle_words)#, savepath='dataset/tacred_test.pkl')

    train_dataloader = DataLoader(train_dataset, batch_size, shuffle=True, num_workers=8, collate_fn=collate_fn)
    val_dataloader = DataLoader(val_dataset, batch_size, num_workers=8, collate_fn=collate_fn)
    test_dataloader = DataLoader(test_dataset, batch_size, num_workers=8, collate_fn=collate_fn)
    logger.debug('dataset and dataloader were initialized')

    id2rel = {v:k for k,v in train_dataset.label2id.items()}
    id2ents = {v:k for k,v in train_dataset.ent2id.items()}
    model_type_dict = {
        "with_ents":[['relation_predictor_adapter'], ['relation_predictor', 'head_ent_predictor', 'tail_ent_predictor'], [id2rel, id2ents, id2ents], RelationPredictorWithEnts, {'id2ents':id2ents, 'id2rel':id2rel}],
        "without_ents":[['relation_predictor_adapter'], ['relation_predictior_adapter'], [id2rel], RelationPredictor, {}],
        "with_ents_proxy":[['relation_predictor_adapter'], ['head_ent_predictor', 'tail_ent_predictor'], [id2ents, id2ents], RelationEncoderProxy, {'label2id':train_dataset.label2id}],
        "cnn":[[], [], [], CNN_classifier, {'kernel_num': 128,'kernel_filters': (3,4,5),'embed_dim': 768,'dropout': 0.1, 'id2label':id2rel}],
        "tfidf":[[], [], [], TFIDFPredictor, {'train_examples':train_dataset.sentence, 'train_ent1':train_dataset.ent1, 'train_ent2':train_dataset.ent2, \
            'type2id':train_dataset.ent2id, 'label2id':train_dataset.label2id}],
        "lstm":[[], [], [], BiLSTM, {'label2id':train_dataset.label2id}]
    }
    adapters, heads, id2labels, model_class, kwargs = model_type_dict[model_type]
    
    logger.debug("Loading encoder...")
    tokenizer, encoder = build_model(model_name=embedding_model, adapters=adapters, num_labels=len(id2rel), classification_heads=heads, headid2label=id2labels)
    special_tokens_dict = {'additional_special_tokens': [train_dataset.ent_start,train_dataset.ent_end]}
    num_added_toks = tokenizer.add_special_tokens(special_tokens_dict)
    encoder.resize_token_embeddings(len(tokenizer))

    logger.success(f"Succesfully loaded encoder from {embedding_model}")


    th.manual_seed(version)
    
    environ['TOKENIZERS_PARALLELISM']='true'
    
    model = model_class.load_from_checkpoint(path2weights, tokenizer=tokenizer, encoder=encoder, embedding_dim=768, \
        relation_types=len(train_dataset.label2id), entity_types=len(train_dataset.ent2id), batch_size=batch_size, **kwargs)
    
    model.to(device)
    model.eval()

    preds = []
    embs = []
    relations = []
    sentences = []
    

    forward_kwargs = {'embed':True} if model_type=='cnn' else {}
    
    with th.no_grad():
        for batch in track(test_dataloader, "Embedding relations..."):
            idx, sentence, ent1, ent2, relation, entity_spans = batch
            embs.append(model.embed(batch).cpu())
            preds.append(model.forward(batch, **forward_kwargs).argmax(-1).cpu())
            relations+=relation
            sentences+=sentence
    
    embeddings = th.concat(embs, dim=0)
    predictions = th.concat(preds, dim=0)
    
    id2labels = [{v:k for k,v in train_dataset.label2id.items()}]

    micro = f1_score(relations, [id2labels[0][p] for p in predictions.tolist()], average='micro')
    macro = f1_score(relations, [id2labels[0][p] for p in predictions.tolist()], average='macro')
    print(f"micro f1: {micro:.4f} {macro:.4f} macro f1")
    print(classification_report(relations, [id2labels[0][p] for p in predictions.tolist()], labels=list(id2labels[0].values()), target_names=list(id2labels[0].values()), zero_division=0.0))

    embedding_dim=768
    class_embeddings = {k:th.tensor([[0.0]*embedding_dim]) for k in train_dataset.label2id.keys()}
    with th.no_grad():
        for i, embedding in enumerate(embeddings):
            class_embeddings[relations[i]] = th.concat([class_embeddings[relations[i]], embedding.unsqueeze(0).cpu()],dim=0)
        for key, embedding in class_embeddings.items():
            class_embeddings[key] = th.mean(embedding, dim=0)
    class_embeddings = {k:v for k,v in class_embeddings.items() if th.sum(v).item()!=0}

    centroids = th.concat([t.unsqueeze(0) for t in class_embeddings.values()], dim=0)
    
    transformer = KNeighborsTransformer(n_neighbors=centroids.shape[0]-1)
    graph = transformer.fit_transform(centroids.numpy())
    centroid_distances_mean = np.sort(graph.todense())[:,:n_neighbors+1].mean()
    centroid_distances_std = np.sort(graph.todense())[:,:n_neighbors+1].std()
    print(f"{centroid_distances_mean:.4f}±{centroid_distances_std:.4f}")

    knn = KNeighborsTransformer(n_neighbors=embeddings.shape[0]-1)
    knn.fit(embeddings.numpy(), relations)
    graph = knn.fit_transform(embeddings.numpy())
    neighbors = np.argsort(graph.todense())[:,1:6]
    classes = np.array([train_dataset.label2id[r] for r in relations])
    predictions = classes[neighbors]
    truth = np.repeat(np.expand_dims(classes,-1),predictions.shape[1], axis=1)
    total_size=predictions.shape[0]*predictions.shape[1]
    error_rate=1-np.sum((truth-predictions)==0)/total_size
    print(f"Overlap is {error_rate}")

    if not n_samples:
        sys.exit()

    cossim = th.nn.CosineSimilarity()
    lexical_similarities = []
    syntax_similarities = []
    content_similarities = []
    embedding_similarities = []
    pairs = list(it.combinations(range(len(sentences)),2))
    pairs = random.sample(pairs, n_samples)

    sent_transformer = SentenceTransformer("distiluse-base-multilingual-cased-v2")
    nlp = stanza.Pipeline(lang='en', models_dir="/srv/tempdd/huthomas/stanfordnlp_resources/", use_gpu=True, tokenize_pretokenized=False)

    for left,right in track(pairs,"Computing pair similarities"):
        l,r = sentences[left], sentences[right]
        nlpl, nlpr = nlp(l), nlp(r)
        lexical_similarities.append(jaccard_similarity(nlpl,nlpr))
        syntax_similarities.append(SABK(nlpl, nlpr))
        content_similarities.append(sentence_transformer_similarity(l,r,sent_transformer))
        embedding_similarities.append(cossim(embeddings[left].unsqueeze(0), embeddings[right].unsqueeze(0)).cpu().item())
    lexicon = spearmanr(lexical_similarities, embedding_similarities)
    syntax = spearmanr(syntax_similarities, embedding_similarities)
    semantics = spearmanr(content_similarities, embedding_similarities)
    print(f"Lexicon : {lexicon.statistic:.4f} ({lexicon.pvalue:.4f}),Syntax : {syntax.statistic:.4f} ({syntax.pvalue:.4f}), Semantics : {semantics.statistic:.4f} ({semantics.pvalue:.4f})")
    
    

    

if __name__ == '__main__':
    router(obj={})

