import numpy as np
from sklearn.metrics import f1_score
from sklearn.decomposition import PCA
from sklearn.feature_extraction.text import TfidfVectorizer
from torchmetrics import F1Score
import torch as th 
import torch.nn as nn
import torch.nn.functional as F
import pytorch_lightning as pl 

import operator as op
import itertools as it, functools as ft 
from collections import Counter
import random

from libraries.strategies import get_indices, to_one_hot

class PositionalEncoding(nn.Module):
    def __init__(self, max_nb_nodes, in_dim, drop_val=0.1):
        super(PositionalEncoding, self).__init__()
        pos = np.arange(0, max_nb_nodes)[:, None]
        idx = np.fromfunction(lambda _,j: j - j % 2, shape=(1, in_dim))
        mask = np.fromfunction(lambda _,j: j % 2 == 0, shape=(1, in_dim))
        pnt = pos / (10000 ** (idx / in_dim))
        val = np.sin(pnt) * mask + np.cos(pnt) * (1 - mask)
        self.drop_layer = nn.Dropout(drop_val)
        self.register_buffer('psne_layer', th.tensor(val).float())

    def forward(self, node_features, node_positions):
        pos = self.psne_layer[node_positions, :]
        return self.drop_layer(node_features + pos)

class NRLModel(nn.Module):
    def __init__(self):
        super(NRLModel, self).__init__()
        self.activation_map = {
            0: nn.Identity(), 
            1: nn.ReLU(), 
            2: nn.LeakyReLU(0.2),
            3: nn.GELU(), 
            4: nn.Tanh(), 
            5: nn.Sigmoid(), 
            6: nn.Softmax(dim=-1)
        }

class MLPModel(NRLModel):
    def __init__(self, layers_config, activations, apply_norm=False):
        super(MLPModel, self).__init__()

        assert isinstance(layers_config, list)
        assert isinstance(activations, list)
        assert len(layers_config)  - 1 == len(activations) 

        self.shapes = list(zip(layers_config[:-1], layers_config[1:]))
        self.activations = activations 
        self.linear_layers = nn.ModuleList([]) 
        
        for index, (in_dim, out_dim) in enumerate(self.shapes):
            linear = nn.Linear(in_features=in_dim, out_features=out_dim)
            nonlinear = self.activation_map[self.activations[index]] 
            if apply_norm and index < len(self.shapes) - 1:
                normalizer = nn.BatchNorm1d(out_dim)
                layer = nn.Sequential(linear, normalizer, nonlinear)
            else:
                layer = nn.Sequential(linear, nonlinear)
            self.linear_layers.append(layer)
    
    def forward(self, X_0):
        X_N = ft.reduce(lambda X_I, LIN: LIN(X_I), self.linear_layers, X_0)
        return X_N 


class RelationPredictorSim(pl.LightningModule):
    def __init__(self, embedding_model, drop_val=0.1, apply_norm=False, learning_rate=1e-3, entity_embedding=None):
        """initialize the model"""
        super(RelationPredictorSim, self).__init__()
        
        self.learning_rate=learning_rate
        self.encoder = embedding_model

        embedding_dim = embedding_model.state_dict()['pooler.dense.weight'].shape[-1]

        
    def configure_optimizers(self):
        optimizer = th.optim.Adam(self.parameters(), lr=self.learning_rate)
        return optimizer

    def training_step(self, train_batch, batch_idx):
        rel_ids, rel_att, desc_ids, desc_att, labels = train_batch
        relations, descriptions = {'input_ids':rel_ids,'attention_mask':rel_att}, {'input_ids':desc_ids,'attention_mask':desc_att}
        batch_size = rel_ids.shape[0]

        rel_embeddings = self.encoder(**relations)['pooler_output']
        # desc_embeddings = self.encoder(**descriptions)['pooler_output']
        # desc_embeddings = ft.reduce(lambda X_I, LIN: LIN(X_I), self.linear_layers_desc, desc_embeddings)

        criterion = nn.CosineEmbeddingLoss()
        # loss = criterion(rel_embeddings, desc_embeddings, truth)
        loss = criterion(rel_embeddings1, rel_embeddings2, truth)
        self.log('train_loss', loss)
        return loss 

    def validation_step(self, val_batch, batch_idx):
        rel_ids, rel_att, desc_ids, desc_att, labels = val_batch
        relations, descriptions = {'input_ids':rel_ids,'attention_mask':rel_att}, {'input_ids':desc_ids,'attention_mask':desc_att}
        batch_size = rel_ids.shape[0]

        rel_embeddings = self.encoder(**relations)['pooler_output']

        criterion = nn.CosineEmbeddingLoss()
        # loss = criterion(rel_embeddings, desc_embeddings, truth)
        loss = criterion(rel_embeddings1, rel_embeddings2, truth)
        self.log('val_loss', loss)
        return loss

class RelationPredictorWithEnts(pl.LightningModule):
    """encode relations through the prediction of their entity types"""
    def __init__(self, tokenizer, encoder, embedding_dim, relation_types, entity_types, batch_size, id2rel, id2ents):
        super().__init__()
        
        self.tokenizer = tokenizer
        self.encoder = encoder
        self.label2id = {v:k for k,v in id2rel.items()}
        self.type2id = {v:k for k,v in id2ents.items()}
        self.ent_criterion, self.rel_criterion = th.nn.CrossEntropyLoss(), th.nn.CrossEntropyLoss()
        self.f1_rel, self.f1_ent1, self.f1_ent2 = F1Score(task='multiclass', num_classes=relation_types), F1Score(task='multiclass', num_classes=entity_types), F1Score(task='multiclass', num_classes=entity_types)
        self.batch_size=batch_size

    def configure_optimizers (self):
        optimizer = th.optim.Adam(self.parameters(), lr=1e-3)
        return optimizer

    def training_step (self, train_batch, batch_idx):
        idx, sentence, ent1, ent2, relation, entity_spans = train_batch
        tokens = self.tokenizer(list(sentence), truncation=True, padding='max_length', return_tensors='pt')
        predictions_e1 = self.encoder(**tokens.to(self.device), head='head_ent_predictor')['logits']
        predictions_e2 = self.encoder(**tokens.to(self.device), head='tail_ent_predictor')['logits']
        predictions_rel = self.encoder(**tokens.to(self.device), head='relation_predictor')['logits']
        #embedding = self.encoder(**tokens.to(self.device))['pooler_output']
        #predictions_e1, predictions_e2 = self.prediction_head_ent1(embedding), self.prediction_head_ent2(embedding)
        e1 = th.tensor([self.type2id[e] for e in ent1], device=self.device)
        e2 = th.tensor([self.type2id[e] for e in ent2], device=self.device)
        target = th.tensor([self.label2id[r] for r in relation], device=self.device)
        ent_loss=(self.ent_criterion(predictions_e1, e1)+self.ent_criterion(predictions_e2, e2))/2.0
        rel_loss = self.rel_criterion(predictions_rel, target)
        self.log('train_rel_loss', rel_loss, batch_size=self.batch_size)
        self.log('train_ent_loss', ent_loss, batch_size=self.batch_size)
        return (ent_loss+rel_loss)/2.0

    def validation_step (self, val_batch, batch_idx):
        idx, sentence, ent1, ent2, relation, entity_spans  = val_batch
        tokens = self.tokenizer(list(sentence), truncation=True, padding='max_length', return_tensors='pt')
        predictions_e1 = self.encoder(**tokens.to(self.device), head='head_ent_predictor')['logits']
        predictions_e2 = self.encoder(**tokens.to(self.device), head='tail_ent_predictor')['logits']
        predictions_rel = self.encoder(**tokens.to(self.device), head='relation_predictor')['logits']
        #predictions_e1, predictions_e2 = self.prediction_head_ent1(embedding), self.prediction_head_ent2(embedding)
        e1 = th.tensor([self.type2id[e] for e in ent1], device=self.device)
        e2 = th.tensor([self.type2id[e] for e in ent2], device=self.device)
        target = th.tensor([self.label2id[r] for r in relation], device=self.device)
        ent_loss=(self.ent_criterion(predictions_e1, e1)+self.ent_criterion(predictions_e2, e2))/2.0
        rel_loss = self.rel_criterion(predictions_rel, target)
        self.log('val_ent_loss', ent_loss, prog_bar=True, batch_size=self.batch_size)
        self.log('val_rel_loss', rel_loss, prog_bar=True, batch_size=self.batch_size)
        self.f1_ent1(predictions_e1, e1)
        self.f1_ent2(predictions_e2, e2)
        self.f1_rel(predictions_rel, target)
        self.log("val_f1_ent1", self.f1_ent1, prog_bar=True, batch_size=self.batch_size)
        self.log("val_f1_ent2", self.f1_ent2, prog_bar=True, batch_size=self.batch_size)
        self.log("val_f1", self.f1_rel, prog_bar=True, batch_size=self.batch_size)
        return (ent_loss+rel_loss)/2.0

    def predict(self,batch):
        _, sentence, _, _, dep_tokens, dep_att_mask, e1, e2, relation = batch
        tokens = self.tokenizer(list(sentence), truncation=True, padding='max_length', return_tensors='pt')
        predictions = self.encoder(**tokens.to(self.device), head='relation_predictor')['logits']
        #predictions_e1, predictions_e2 = self.prediction_head_ent1(embedding), self.prediction_head_ent2(embedding)
        return predictions
    
    def forward(self, batch):
        with th.no_grad():
            idx, sentence, ent1, ent2, relation, entity_spans  = batch
            tokens = self.tokenizer(list(sentence), truncation=True, padding='max_length', return_tensors='pt')
            predictions = self.encoder(**tokens.to(self.device), head='relation_predictor')['logits']
        return predictions
    
    def embed(self, batch):
        with th.no_grad():
            idx, sentence, ent1, ent2, relation, entity_spans  = batch
            tokens = self.tokenizer(list(sentence), truncation=True, padding='max_length', return_tensors='pt')
            embeddings = self.encoder(**tokens.to(self.device), head='relation_predictor', output_hidden_states=True)['hidden_states'][-1][:,0,:]
        return embeddings

class RelationPredictor(pl.LightningModule):
    """encode relations through the prediction of their entity types"""
    def __init__(self, tokenizer, encoder, embedding_dim, relation_types, batch_size, entity_types=None):
        super().__init__()
        
        self.tokenizer = tokenizer
        self.encoder = encoder
        self.label2id = {v:k for k,v in self.encoder.get_labels_dict().items()}
        self.batch_size = batch_size
        
        #self.prediction_head = th.nn.Sequential(th.nn.Linear(embedding_dim, relation_types))
        self.criterion = th.nn.CrossEntropyLoss()
        self.f1 = F1Score(task='multiclass', num_classes=relation_types)
        
    def configure_optimizers (self):
        optimizer = th.optim.Adam(self.parameters(), lr=1e-3)
        return optimizer

    def training_step (self, train_batch, batch_idx):
        idx, sentence, ent1, ent2, relation, entity_spans = train_batch
        tokens = self.tokenizer(list(sentence), truncation=True, padding='max_length', return_tensors='pt')
        embedding = self.encoder(**tokens.to(self.device))['logits']
        predictions = embedding #self.prediction_head(embedding)
        target = th.tensor([self.label2id[r] for r in relation], device=self.device)
        loss=self.criterion(predictions, target)
        self.log('train_loss', loss, batch_size=self.batch_size)
        return loss

    def validation_step (self, val_batch, batch_idx):
        idx, sentence, ent1, ent2, relation, entity_spans = val_batch
        tokens = self.tokenizer(list(sentence), truncation=True, padding='max_length', return_tensors='pt')
        embedding = self.encoder(**tokens.to(self.device))['logits']
        predictions = embedding #self.prediction_head(embedding)
        target = th.tensor([self.label2id[r] for r in relation], device=self.device)
        loss=self.criterion(predictions, target)
        self.log('val_loss', loss, prog_bar=True, batch_size=self.batch_size)
        self.f1(predictions, target)
        self.log("val_f1", self.f1, prog_bar=True, batch_size=self.batch_size)
        return loss
    
    def test_step (self, test_batch, batch_idx):
        idx, sentence, ent1, ent2, relation, entity_spans = test_batch
        tokens = self.tokenizer(list(sentence), truncation=True, padding='max_length', return_tensors='pt')
        embedding = self.encoder(**tokens.to(self.device))['logits']
        predictions = embedding #self.prediction_head(embedding)
        metrics = classification_report(relation.cpu(), predictions.argmax(-1).cpu(), output_dict=True, zero_division=0.0, target_names=list(train_dataset.rel2id.keys()),labels=list(range(len(train_dataset.rel2id))))
        self.log_dict(metrics)
        return metrics
    
    def forward(self, batch):
        idx, sentence, ent1, ent2, relation, entity_spans = batch
        tokens = self.tokenizer(list(sentence), truncation=True, padding='max_length', return_tensors='pt')
        embedding = self.encoder(**tokens.to(self.device))['logits']
        return embedding
    
    def embed(self, batch):
        idx, sentence, ent1, ent2, relation, entity_spans = batch
        tokens = self.tokenizer(list(sentence), truncation=True, padding='max_length', return_tensors='pt').to(self.device)
        embedding = self.encoder(**tokens.to(self.device), output_hidden_states=True)['hidden_states'][-1][:,0,:]
        return embedding
    
class RelationPredictorNoAdapter(pl.LightningModule):
    """encode relations through the prediction of their entity types"""
    def __init__(self, tokenizer, encoder, embedding_dim, relation_types, batch_size, label2id, entity_types=None):
        super().__init__()
        
        self.tokenizer = tokenizer
        self.encoder = encoder
        self.label2id = label2id
        self.batch_size = batch_size
        
        self.prediction_head = th.nn.Sequential(th.nn.Linear(embedding_dim, embedding_dim), th.nn.Tanh(), th.nn.Linear(embedding_dim, relation_types))
        self.criterion = th.nn.CrossEntropyLoss()
        self.f1 = F1Score(task='multiclass', num_classes=relation_types)
        
    def configure_optimizers (self):
        optimizer = th.optim.Adam(self.parameters(), lr=1e-3)
        return optimizer

    def training_step (self, train_batch, batch_idx):
        idx, sentence, ent1, ent2, relation, entity_spans = train_batch
        tokens = self.tokenizer(list(sentence), truncation=True, padding='max_length', return_tensors='pt')
        embedding = self.encoder(**tokens.to(self.device), output_hidden_states=True)['hidden_states'][-1][:,0,:]
        predictions = self.prediction_head(embedding)
        target = th.tensor([self.label2id[r] for r in relation], device=self.device)
        loss=self.criterion(predictions, target)
        self.log('train_loss', loss, batch_size=self.batch_size)
        return loss

    def validation_step (self, val_batch, batch_idx):
        idx, sentence, ent1, ent2, relation, entity_spans = val_batch
        tokens = self.tokenizer(list(sentence), truncation=True, padding='max_length', return_tensors='pt')
        embedding = self.encoder(**tokens.to(self.device), output_hidden_states=True)['hidden_states'][-1][:,0,:]
        predictions = self.prediction_head(embedding)
        target = th.tensor([self.label2id[r] for r in relation], device=self.device)
        loss=self.criterion(predictions, target)
        self.log('val_loss', loss, prog_bar=True, batch_size=self.batch_size)
        self.f1(predictions, target)
        self.log("val_f1", self.f1, prog_bar=True, batch_size=self.batch_size)
        return loss
    
    def test_step (self, test_batch, batch_idx):
        idx, sentence, ent1, ent2, relation, entity_spans = test_batch
        tokens = self.tokenizer(list(sentence), truncation=True, padding='max_length', return_tensors='pt')
        embedding = self.encoder(**tokens.to(self.device), output_hidden_states=True)['hidden_states'][-1][:,0,:]
        predictions = self.prediction_head(embedding)
        metrics = classification_report(relation.cpu(), predictions.argmax(-1).cpu(), output_dict=True, zero_division=0.0, target_names=list(train_dataset.rel2id.keys()),labels=list(range(len(train_dataset.rel2id))))
        self.log_dict(metrics)
        return metrics
    
    def forward(self, batch):
        idx, sentence, ent1, ent2, relation, entity_spans = batch
        tokens = self.tokenizer(list(sentence), truncation=True, padding='max_length', return_tensors='pt')
        embedding = self.encoder(**tokens.to(self.device), output_hidden_states=True)['hidden_states'][-1][:,0,:]
        predictions = self.prediction_head(embedding)
        return predictions
    
    def embed(self, batch):
        idx, sentence, ent1, ent2, relation, entity_spans = batch
        tokens = self.tokenizer(list(sentence), truncation=True, padding='max_length', return_tensors='pt').to(self.device)
        embedding = self.encoder(**tokens.to(self.device), output_hidden_states=True)['hidden_states'][-1][:,0,:]
        return embedding

class RelationEncoderProxy(pl.LightningModule):
    """encode relations through the prediction of their entity types"""
    def __init__(self, tokenizer, encoder, embedding_dim, entity_types, batch_size, relation_types, label2id):
        super().__init__()
        
        self.tokenizer = tokenizer
        self.encoder = encoder
        self.batch_size = batch_size
        self.prediction_head = th.nn.Sequential(th.nn.Linear(embedding_dim,embedding_dim),th.nn.Tanh(),th.nn.Linear(embedding_dim, relation_types))
        #self.prediction_head_ent2 = th.nn.Sequential(th.nn.Linear(embedding_dim,embedding_dim),th.nn.Tanh(),th.nn.Linear(embedding_dim, entity_types))
        self.criterion = th.nn.CrossEntropyLoss()
        self.relation_criterion = th.nn.CrossEntropyLoss()
        self.f1_ent1, self.f1_ent2 = F1Score(task='multiclass', num_classes=entity_types), F1Score(task='multiclass', num_classes=entity_types)
        self.f1 = F1Score(task='multiclass', num_classes=relation_types)
        self.type2id = {v:k for k,v in self.encoder.get_labels_dict().items()}
        self.label2id = label2id
        self.encoder_no_grad=False
        
    def configure_optimizers (self):
        optimizer = th.optim.Adam(self.parameters(), lr=1e-3)
        return optimizer

    def training_step (self, train_batch, batch_idx):
        idx, sentence, ent1, ent2, relation, entity_spans = train_batch
        tokens = self.tokenizer(list(sentence), truncation=True, padding='max_length', return_tensors='pt')
        if self.encoder_no_grad:
            embeddings = self.encoder(**tokens.to(self.device), output_hidden_states=True)['hidden_states'][-1][:,0,:]
            predictions = self.prediction_head(embeddings)
            targets = th.tensor([self.label2id[r] for r in relation], device=self.device)
            loss=self.relation_criterion(predictions, targets)
            self.log('train_loss_relation', loss, prog_bar=True, batch_size=self.batch_size)
        else:
            predictions_e1 = self.encoder(**tokens.to(self.device), head='head_ent_predictor')['logits']
            predictions_e2 = self.encoder(**tokens.to(self.device), head='tail_ent_predictor')['logits']
            #embedding = self.encoder(**tokens.to(self.device))['pooler_output']
            #predictions_e1, predictions_e2 = self.prediction_head_ent1(embedding), self.prediction_head_ent2(embedding)
            e1 = th.tensor([self.type2id[e] for e in ent1], device=self.device)
            e2 = th.tensor([self.type2id[e] for e in ent2], device=self.device)
            loss=(self.criterion(predictions_e1, e1)+self.criterion(predictions_e2, e2))/2.0
            self.log('train_loss', loss, batch_size=self.batch_size)
        return loss

    def validation_step (self, val_batch, batch_idx):
        idx, sentence, ent1, ent2, relation, entity_spans = val_batch
        tokens = self.tokenizer(list(sentence), truncation=True, padding='max_length', return_tensors='pt')
        if self.encoder_no_grad:
            embeddings = self.encoder(**tokens.to(self.device), output_hidden_states=True)['hidden_states'][-1][:,0,:]

            predictions = self.prediction_head(embeddings)
            targets = th.tensor([self.label2id[r] for r in relation], device=self.device)
            loss=self.relation_criterion(predictions, targets)
            self.log('val_loss_relation', loss, prog_bar=True, batch_size=self.batch_size)
            f1 = self.f1(predictions, targets)
            self.log("val_f1_relation", self.f1, prog_bar=True, batch_size=self.batch_size)
        else:
            predictions_e1 = self.encoder(**tokens.to(self.device), head='head_ent_predictor')['logits']
            predictions_e2 = self.encoder(**tokens.to(self.device), head='tail_ent_predictor')['logits']
            #predictions_e1, predictions_e2 = self.prediction_head_ent1(embedding), self.prediction_head_ent2(embedding)
            e1 = th.tensor([self.type2id[e] for e in ent1], device=self.device)
            e2 = th.tensor([self.type2id[e] for e in ent2], device=self.device)
            loss=(self.criterion(predictions_e1, e1)+self.criterion(predictions_e2, e2))/2.0
            self.log('val_loss', loss, prog_bar=True, batch_size=self.batch_size)
            f1e1 = self.f1_ent1(predictions_e1, e1)
            f1e2 = self.f1_ent2(predictions_e2, e2)
            self.log("val_f1_ent1", self.f1_ent1, prog_bar=True, batch_size=self.batch_size)
            self.log("val_f1_ent2", self.f1_ent2, prog_bar=True, batch_size=self.batch_size)
            val_f1 = float((f1e1+f1e2)/2.0)
            self.log("val_f1", val_f1, prog_bar=True, batch_size=self.batch_size)
        return loss
    
    def test_step (self, test_batch, batch_idx):
        _, sentence, _, _, dep_tokens, dep_att_mask, e1, e2, relation = test_batch
        tokens = self.tokenizer(list(sentence), truncation=True, padding='max_length', return_tensors='pt')
        predictions_e1 = self.encoder(**tokens.to(self.device), head='head_ent_predictor')['logits']
        predictions_e2 = self.encoder(**tokens.to(self.device), head='tail_ent_predictor')['logits']
        
        #return loss
    
    def forward(self, batch):
        idx, sentence, ent1, ent2, relation, entity_spans = batch
        tokens = self.tokenizer(list(sentence), truncation=True, padding='max_length', return_tensors='pt')
        embedding = self.encoder(**tokens.to(self.device), output_hidden_states=True)['hidden_states'][-1][:,0,:]
        predictions = self.prediction_head(embedding)
        return predictions
    
    def predict(self, batch):
        _, sentence, _, _, dep_tokens, dep_att_mask, e1, e2, relation = batch
        tokens = self.tokenizer(list(sentence), truncation=True, padding='max_length', return_tensors='pt')
        #predictions_e1, predictions_e2 = self.prediction_head_ent1(embedding), self.prediction_head_ent2(embedding)
        embedding = self.encoder(**tokens.to(self.device), output_hidden_states=True)['hidden_states'][-1][:,0,:]
        return embedding

    def encoder_disable_grad(self):
        for param in self.encoder.parameters():
            param.requires_grad=False
        self.encoder_no_grad=True

    def embed(self, batch):
        idx, sentence, ent1, ent2, relation, entity_spans = batch
        tokens = self.tokenizer(list(sentence), truncation=True, padding='max_length', return_tensors='pt')
        embeddings = self.encoder(**tokens.to(self.device), output_hidden_states=True)['hidden_states'][-1][:,0,:]
        return embeddings

class CNN_classifier(pl.LightningModule):
    """
    A CNN sentence/text classification model based on the paper: Convolutional Neural Networks for Sentence Classification.
    Implementation is a generalization of https://github.com/Shawn1993/cnn-text-classification-pytorch/blob/master/model.py
    and it is modififed according to PyTorch Lightning.
    """
    def __init__(self, **kwargs):
        super(CNN_classifier, self).__init__()
        # store paramters from kwargs
        self.batch_size = kwargs['batch_size']
        self.C_out = kwargs['kernel_num']
        self.embed_dim = kwargs['embed_dim']
        self.kernel_filters = kwargs['kernel_filters']
        self.lr = 1e-3
        self.tokenizer = kwargs['tokenizer']
        self.encoder = kwargs['encoder']
        self.label2id = {v:k for k,v in kwargs['id2label'].items()}
        self.id2label = kwargs['id2label']
        self.f1 = F1Score(task='multiclass', num_classes=len(self.label2id))

        for param in self.encoder.parameters():
            param.requires_grad=False
        self.embeddings = self.encoder.embeddings

        # define convolutional layers
        self.conv_layers = nn.ModuleDict({
            f'conv{i+1}': self._init_conv_layer(filter_size) for i, filter_size in enumerate(self.kernel_filters) 
        })
        
        # define FC layer
        self.projection=nn.Linear(
            in_features=len(self.conv_layers) * self.C_out,
            out_features=768
        )

        self.fc = nn.Sequential(
        nn.Linear(
            in_features=768,
            out_features=768
        ),
        nn.Tanh(),
        nn.Linear(
            in_features=768,
            out_features=len(self.label2id)
        ),
        )
        # define drop-out layer
        self.dropout = nn.Dropout(kwargs['dropout'])

    def forward(self, x, embed=False):
        """
        :param x: input tensor of size [batch_size, seq_len, embed_dim ]
        """
        if embed:
            idx, sentence, ent1, ent2, relation, entity_spans = x
            tokens = self.tokenizer(list(sentence), return_tensors='pt', truncation=True, padding='max_length')
            embeddings =self.embeddings(tokens['input_ids'].to(self.device))
            output = self.forward(embeddings)
            return output

        x = x.unsqueeze(1) # [batch_size, in_channel, seq_len, embed_dim]              
        x = self.dropout(
                th.cat(
                    tuple(self._conv_forward(x, i+1) for i,_ in enumerate(self.conv_layers.keys())),
                    dim=1
                )
        )
        x = self.projection(x)
        x = self.fc(x)
        return x
    
    def training_step(self, batch, batch_idx):
        idx, sentence, ent1, ent2, relation, entity_spans = batch
        tokens = self.tokenizer(list(sentence), return_tensors='pt', truncation=True, padding='max_length')
        embeddings =self.embeddings(tokens['input_ids'].to(self.device))
        output = self.forward(embeddings)
        target = th.tensor([self.label2id[r] for r in relation], device=self.device)
        loss = F.cross_entropy(output, target.long())
        self.log('train_loss', loss, batch_size=self.batch_size)
        return {'loss': loss}
    
    def validation_step(self, batch, batch_idx):
        idx, sentence, ent1, ent2, relation, entity_spans = batch
        tokens = self.tokenizer(list(sentence), return_tensors='pt', truncation=True, padding='max_length')
        embeddings =self.embeddings(tokens['input_ids'].to(self.device))
        output = self.forward(embeddings)
        target = th.tensor([self.label2id[r] for r in relation], device=self.device)
        loss = F.cross_entropy(output, target.long())
        self.log('val_loss', loss, batch_size=self.batch_size, prog_bar=True)
        self.f1(output, target.long())
        self.log('val_f1', self.f1, batch_size=self.batch_size, prog_bar=True)
        return {'val_loss': loss}
    
    def validation_epoch_end(self, outputs):
        avg_loss = th.stack([x['val_loss'] for x in outputs]).mean()
        tensorboard_logs = {'val_loss': avg_loss}
        return {
            'avg_val_loss': avg_loss,
            'log': tensorboard_logs
        }
    
    def predict_step(self, batch, batch_idx):
        idx, sentence, ent1, ent2, relation, entity_spans = batch
        tokens = self.tokenizer(list(sentence), return_tensors='pt', truncation=True, padding='max_length')
        embeddings =self.embeddings(tokens['input_ids'].to(self.device))
        output = self.forward(embeddings)
        return output.cpu()
    
    def test_epoch_end(self, outputs):
        avg_loss = th.stack([x['test_loss'] for x in outputs]).mean()
        tensorboard_logs = {'test_loss': avg_loss}
        return {
            'avg_test_loss': avg_loss,
            'log': tensorboard_logs
        }
    
    def configure_optimizers(self):
        optimizer = th.optim.Adam(self.parameters(), lr=self.lr)
        return optimizer
    

    def _init_conv_layer(self, filter_size):
        """
        Helper function: a function to instantiate 2D convolutional layers
        :param filter_size: an integer determining a number of words kernel size should span over.
        """
        return nn.Conv2d(
            in_channels=1,
            out_channels=self.C_out,
            kernel_size=(filter_size, self.embed_dim)
        )

    def _conv_forward(self, x, i):
        """
        Helper function: Forward for convolutional layer accompanied by relu and max_pool operation
        :param x: input tensor of size [batch_size, in_channel, seq_len, embed_dim]
        :param i: an integer denoting which convolution layer should be used
        """
        x = F.relu(self.conv_layers[f'conv{i}'](x)).squeeze(3) # [batch_size, in_channel, seq_len]
        return F.max_pool1d(x, x.size(2)).squeeze(2) # [batch_size, in_channel]
    
    def embed(self, batch):
        idx, sentence, ent1, ent2, relation, entity_spans = batch
        tokens = self.tokenizer(list(sentence), return_tensors='pt', truncation=True, padding='max_length')
        x =self.embeddings(tokens['input_ids'].to(self.device))
        x = x.unsqueeze(1) # [batch_size, in_channel, seq_len, embed_dim]              
        x = self.dropout(
                th.cat(
                    tuple(self._conv_forward(x, i+1) for i,_ in enumerate(self.conv_layers.keys())),
                    dim=1
                )
        )
        x=self.projection(x)
        return x

class TFIDFPredictor(pl.LightningModule):
    """encode relations through the prediction of their entity types"""
    def __init__(self, embedding_dim, relation_types, entity_types, batch_size, train_examples, train_ent1, train_ent2, type2id, label2id, tokenizer=None, encoder=None):
        super().__init__()
        self.entity_types = entity_types
        self.type2id = type2id
        self.label2id = label2id
        train_ent1 = [self.type2id[e] for e in train_ent1]
        train_ent2 = [self.type2id[e] for e in train_ent2]
        self.prediction_head=th.nn.Sequential(th.nn.Linear(embedding_dim, 768), th.nn.Tanh(), \
            th.nn.Linear(768,relation_types))
        self.criterion = th.nn.CrossEntropyLoss()
        self.f1 = F1Score(task='multiclass', num_classes=relation_types)
        self.vectorizer = TfidfVectorizer()
        print('fitting TFIDF...')
        self.tfidf_train_examples = self.vectorizer.fit_transform(train_examples)
        print('Done ! Fitting PCA...')
        ent1vec, ent2vec = to_one_hot(np.array(train_ent1), self.entity_types), to_one_hot(np.array(train_ent2), self.entity_types)
        self.tfidf_train_examples = self.tfidf_train_examples.toarray()
        self.tfidf_train_examples = np.concatenate([self.tfidf_train_examples, ent1vec, ent2vec], axis=1)
        self.projection = PCA(n_components=embedding_dim)

        self.projection.fit(self.tfidf_train_examples)
        print('Done')
        self.batch_size = batch_size
    def configure_optimizers (self):
        optimizer = th.optim.Adam(self.parameters(), lr=1e-3)
        return optimizer

    def training_step (self, train_batch, batch_idx):
        idx, sentence, ent1, ent2, relation, entity_spans = train_batch
        embeddings = self.vectorizer.transform(sentence).toarray()
        ent1 = [self.type2id[e] for e in ent1]
        ent2 = [self.type2id[e] for e in ent2]
        ent1vec, ent2vec = to_one_hot(np.array(ent1), self.entity_types), to_one_hot(np.array(ent2), self.entity_types)
        embeddings = np.concatenate([embeddings, ent1vec, ent2vec], axis=1)
        embeddings = th.tensor(self.projection.transform(embeddings), device= self.device).float()
        predictions = self.prediction_head(embeddings)
        target = th.tensor([self.label2id[r] for r in relation], device=self.device)
        loss=self.criterion(predictions, target)
        self.log('train_loss', loss, prog_bar=True, batch_size=self.batch_size)
        return loss

    def validation_step (self, val_batch, batch_idx):
        idx, sentence, ent1, ent2, relation, entity_spans = val_batch
        embeddings = self.vectorizer.transform(sentence).toarray()
        ent1 = [self.type2id[e] for e in ent1]
        ent2 = [self.type2id[e] for e in ent2]
        ent1vec, ent2vec = to_one_hot(np.array(ent1), self.entity_types), to_one_hot(np.array(ent2), self.entity_types)
        embeddings = np.concatenate([embeddings, ent1vec, ent2vec], axis=1)
        embeddings = th.tensor(self.projection.transform(embeddings), device= self.device).float()
        predictions = self.prediction_head(embeddings)
        target = th.tensor([self.label2id[r] for r in relation], device=self.device)
        loss=self.criterion(predictions, target)
        self.log('val_loss', loss, prog_bar=True, batch_size=self.batch_size)
        self.f1(predictions, target)
        self.log("val_f1", self.f1, prog_bar=True, batch_size=self.batch_size)
        return loss
    
    def forward(self, batch):
        idx, sentence, ent1, ent2, relation, entity_spans = batch
        embeddings = self.vectorizer.transform(sentence).toarray()
        ent1 = [self.type2id[e] for e in ent1]
        ent2 = [self.type2id[e] for e in ent2]
        ent1vec, ent2vec = to_one_hot(np.array(ent1), self.entity_types), to_one_hot(np.array(ent2), self.entity_types)
        embeddings = np.concatenate([embeddings, ent1vec, ent2vec], axis=1)
        embeddings = th.tensor(self.projection.transform(embeddings), device= self.device).float()
        predictions = self.prediction_head(embeddings)
        #print(predictions, labels)
        return predictions

    def predict(self, batch):
        idx, sentence, ent1, ent2, relation, entity_spans = batch
        embeddings = self.vectorizer.transform(sentence).toarray()
        ent1vec, ent2vec = to_one_hot(np.array(ent1), self.entity_types), to_one_hot(np.array(ent2), self.entity_types)
        embeddings = np.concatenate([embeddings, ent1, ent2], -1)
        embeddings = th.tensor(self.projection.transform(embeddings), device= self.device).float()
        predictions = self.prediction_head(embeddings)
        #print(predictions, labels)
        return predictions
    
    def embed(self, batch):
        idx, sentence, ent1, ent2, relation, entity_spans = batch
        embeddings = self.vectorizer.transform(sentence).toarray()
        ent1 = [self.type2id[e] for e in ent1]
        ent2 = [self.type2id[e] for e in ent2]
        ent1vec, ent2vec = to_one_hot(np.array(ent1), self.entity_types), to_one_hot(np.array(ent2), self.entity_types)
        embeddings = np.concatenate([embeddings, ent1vec, ent2vec], axis=1)
        embeddings = th.tensor(self.projection.transform(embeddings), device= self.device).float()
        return embeddings
    
class BiLSTM(pl.LightningModule):
    """encode relations through the prediction of their entity types"""
    def __init__(self, tokenizer,encoder, embedding_dim, relation_types, batch_size, label2id, entity_types=None):
        super().__init__()
        
        self.tokenizer = tokenizer
        self.embeddings = encoder.embeddings.word_embeddings
        for param in self.embeddings.parameters():
            param.requires_grad = False
        self.embeddings.weight[self.tokenizer.pad_token_id,:]=th.zeros(self.embeddings.embedding_dim)
        self.encoder = th.nn.LSTM(768, 384, batch_first=True, bidirectional=True)
        self.label2id = label2id
        self.batch_size = batch_size
        
        self.prediction_head = th.nn.Sequential(th.nn.Linear(768,768), th.nn.Tanh(), th.nn.Linear(768,relation_types))
        self.criterion = th.nn.CrossEntropyLoss()
        self.f1 = F1Score(task='multiclass', num_classes=relation_types)
        
    def configure_optimizers (self):
        optimizer = th.optim.Adam(self.parameters(), lr=1e-3)
        return optimizer

    def training_step (self, train_batch, batch_idx):
        idx, sentence, ent1, ent2, relation, entity_spans = train_batch
        
        tokens = self.tokenizer(list(sentence), truncation=True, padding='max_length', return_tensors='pt')
        embeddings = self.embeddings(tokens['input_ids'].to(self.device))
        _, (embeddings, _) = self.encoder(embeddings)
        embeddings = embeddings.permute(1,0,2).reshape((len(sentence),768))
        predictions = self.prediction_head(embeddings)
        target = th.tensor([self.label2id[r] for r in relation], device=self.device)
        loss=self.criterion(predictions, target)
        self.log('train_loss', loss, batch_size=self.batch_size)
        return loss

    def validation_step (self, val_batch, batch_idx):
        idx, sentence, ent1, ent2, relation, entity_spans = val_batch
        tokens = self.tokenizer(list(sentence), truncation=True, padding='max_length', return_tensors='pt')
        embeddings = self.embeddings(tokens['input_ids'].to(self.device))
        _, (embeddings, _) = self.encoder(embeddings)
        embeddings = embeddings.permute(1,0,2).reshape((len(sentence),768))
        predictions = self.prediction_head(embeddings)
        target = th.tensor([self.label2id[r] for r in relation], device=self.device)
        loss=self.criterion(predictions, target)
        self.log('val_loss', loss, prog_bar=True, batch_size=self.batch_size)
        self.f1(predictions, target)
        self.log("val_f1", self.f1, prog_bar=True, batch_size=self.batch_size)
        return loss
    
    def test_step (self, test_batch, batch_idx):
        idx, sentence, ent1, ent2, relation, entity_spans = test_batch
        tokens = self.tokenizer(list(sentence), truncation=True, padding='max_length', return_tensors='pt')
        embeddings = self.embeddings(tokens['input_ids'].to(self.device))
        _, (embeddings, _) = self.encoder(embeddings)
        embeddings = embeddings.permute(1,0,2).reshape((len(sentence),768))
        predictions = self.prediction_head(embeddings)
        metrics = classification_report(relation.cpu(), predictions.argmax(-1).cpu(), output_dict=True, zero_division=0.0, target_names=list(train_dataset.rel2id.keys()),labels=list(range(len(train_dataset.rel2id))))
        self.log_dict(metrics)
        return metrics
    
    def forward(self, batch):
        idx, sentence, ent1, ent2, relation, entity_spans = batch
        tokens = self.tokenizer(list(sentence), truncation=True, padding='max_length', return_tensors='pt')
        embeddings = self.embeddings(tokens['input_ids'].to(self.device))
        _, (embeddings, _) = self.encoder(embeddings)
        embeddings = embeddings.permute(1,0,2).reshape((len(sentence),768))
        predictions = self.prediction_head(embeddings)
        return predictions
    
    def embed(self, batch):
        idx, sentence, ent1, ent2, relation, entity_spans = batch
        tokens = self.tokenizer(list(sentence), truncation=True, padding='max_length', return_tensors='pt').to(self.device)
        embeddings = self.embeddings(tokens['input_ids'].to(self.device))
        _, (embeddings, _) = self.encoder(embeddings)
        embeddings = embeddings.permute(1,0,2).reshape((len(sentence),768))
        return embeddings
    
class NaivePredictor(pl.LightningModule):
    """encode relations through the prediction of their entity types"""
    def __init__(self, embedding_dim, relation_types, entity_types, batch_size, train_labels, train_ent1, train_ent2, type2id, label2id, tokenizer=None, encoder=None):
        super().__init__()
        self.entity_types = entity_types
        self.type2id = type2id
        self.label2id = label2id
        train_ent1 = [self.type2id[e] for e in train_ent1]
        train_ent2 = [self.type2id[e] for e in train_ent2]
        train_labels = [self.label2id[r] for r in train_labels]
        self.label_ents = {}
        for i, label in enumerate(train_labels):
            e1, e2 = train_ent1[i], train_ent2[i]
            ent_key = str(e1)+'_'+str(e2)
            if not label in self.label_ents:
                self.label_ents[label]=[]
            self.label_ents[label].append(ent_key)
        self.label_ents = {k:Counter(v) for k,v in self.label_ents.items()}

        self.f1 = F1Score(task='multiclass', num_classes=relation_types)

        self.batch_size = batch_size
    def configure_optimizers (self):
        return None

    def test_step (self, test_batch, batch_idx):
        idx, sentence, ent1, ent2, relation, entity_spans = test_batch
        ent1 = [self.type2id[e] for e in ent1]
        ent2 = [self.type2id[e] for e in ent2]
        predictions = []
        for e1, e2 in zip(ent1, ent2):
            ent_key = e1+'_'+e2
            if ent_key in self.label_ents:
                predictions.append(random.choices(list(self.label_ents[ent_key].keys()), weights=list(self.label_ents[ent_key].values()), k=1))
            else:
                print("OOV")
                predictions.append(random.choice(list(self.label2id.values())))
        predictions = th.tensor(predictions, device=self.device)
    
    def forward(self, batch):
        idx, sentence, ent1, ent2, relation, entity_spans = batch
        ent1 = [self.type2id[e] for e in ent1]
        ent2 = [self.type2id[e] for e in ent2]
        predictions = []
        for e1, e2 in zip(ent1, ent2):
            ent_key = str(e1)+'_'+str(e2)
            if ent_key in self.label_ents:
                predictions.append(random.choices(list(self.label_ents[ent_key].keys()), weights=list(self.label_ents[ent_key].values()), k=1))
            else:
                predictions.append(random.choice(list(self.label2id.values())))
        predictions = th.tensor(predictions, device=self.device)
        return predictions

    def predict(self, batch):
        idx, sentence, ent1, ent2, relation, entity_spans = batch
        embeddings = self.vectorizer.transform(sentence).toarray()
        ent1vec, ent2vec = to_one_hot(np.array(ent1), self.entity_types), to_one_hot(np.array(ent2), self.entity_types)
        embeddings = np.concatenate([embeddings, ent1, ent2], -1)
        embeddings = th.tensor(self.projection.transform(embeddings), device= self.device).float()
        predictions = self.prediction_head(embeddings)
        #print(predictions, labels)
        return predictions
    
    def embed(self, batch):
        idx, sentence, ent1, ent2, relation, entity_spans = batch
        embeddings = self.vectorizer.transform(sentence).toarray()
        ent1 = [self.type2id[e] for e in ent1]
        ent2 = [self.type2id[e] for e in ent2]
        ent1vec, ent2vec = to_one_hot(np.array(ent1), self.entity_types), to_one_hot(np.array(ent2), self.entity_types)
        embeddings = np.concatenate([embeddings, ent1vec, ent2vec], axis=1)
        embeddings = th.tensor(self.projection.transform(embeddings), device= self.device).float()
        return embeddings
    
class LUKERelationPredictor(pl.LightningModule):
    """encode relations through the prediction of their entity types"""
    def __init__(self, tokenizer, encoder, embedding_dim, relation_types, batch_size, label2id, entity_types=None):
        super().__init__()
        
        self.tokenizer = tokenizer
        self.encoder = encoder
        self.label2id = label2id
        self.batch_size = batch_size
        
        self.prediction_head = th.nn.Sequential(th.nn.Linear(embedding_dim, embedding_dim), th.nn.Tanh(),th.nn.Linear(embedding_dim, relation_types))
        self.criterion = th.nn.CrossEntropyLoss()
        self.f1 = F1Score(task='multiclass', num_classes=relation_types)
        
    def configure_optimizers (self):
        optimizer = th.optim.Adam(self.parameters(), lr=1e-3)
        return optimizer

    def training_step (self, train_batch, batch_idx):
        idx, sentence, ent1, ent2, relation, entity_spans = train_batch
        tokens = self.tokenizer(list(sentence), truncation=True, padding='max_length', return_tensors='pt', entity_spans=entity_spans)
        embedding = self.encoder(**tokens.to(self.device), output_hidden_states=True)['hidden_states'][-1][:,0,:]
        predictions = embedding #self.prediction_head(embedding)
        target = th.tensor([self.label2id[r] for r in relation], device=self.device)
        loss=self.criterion(predictions, target)
        self.log('train_loss', loss, batch_size=self.batch_size)
        return loss

    def validation_step (self, val_batch, batch_idx):
        idx, sentence, ent1, ent2, relation, entity_spans = val_batch
        tokens = self.tokenizer(list(sentence), truncation=True, padding='max_length', return_tensors='pt', entity_spans=entity_spans)
        embedding = self.encoder(**tokens.to(self.device), output_hidden_states=True)['hidden_states'][-1][:,0,:]
        predictions = self.prediction_head(embedding)
        target = th.tensor([self.label2id[r] for r in relation], device=self.device)
        loss=self.criterion(predictions, target)
        self.log('val_loss', loss, prog_bar=True, batch_size=self.batch_size)
        self.f1(predictions, target)
        self.log("val_f1", self.f1, prog_bar=True, batch_size=self.batch_size)
        return loss
    
    def test_step (self, test_batch, batch_idx):
        idx, sentence, ent1, ent2, relation, entity_spans = test_batch
        tokens = self.tokenizer(list(sentence), truncation=True, padding='max_length', return_tensors='pt', entity_spans=entity_spans)
        embedding = self.encoder(**tokens.to(self.device), output_hidden_states=True)['hidden_states'][-1][:,0,:]
        predictions = self.prediction_head(embedding)
        metrics = classification_report(relation.cpu(), predictions.argmax(-1).cpu(), output_dict=True, zero_division=0.0, target_names=list(train_dataset.rel2id.keys()),labels=list(range(len(train_dataset.rel2id))))
        self.log_dict(metrics)
        return metrics
    
    def forward(self, batch):
        idx, sentence, ent1, ent2, relation, entity_spans = batch
        tokens = self.tokenizer(list(sentence), truncation=True, padding='max_length', return_tensors='pt', entity_spans=entity_spans)
        embedding = self.encoder(**tokens.to(self.device), output_hidden_states=True)['hidden_states'][-1][:,0,:]
        predictions = self.prediction_head(embedding)
        return predictions
    
    def embed(self, batch):
        idx, sentence, ent1, ent2, relation, entity_spans = batch
        tokens = self.tokenizer(list(sentence), truncation=True, padding='max_length', return_tensors='pt', entity_spans=entity_spans)
        embedding = self.encoder(**tokens.to(self.device), output_hidden_states=True)['hidden_states'][-1][:,0,:]
        return embedding