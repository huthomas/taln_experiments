from torch.utils.data import Dataset
from itertools import chain
import json
from os.path import join

class RelationDataset(Dataset):
    """A class to load FewRel relationships for model training and testing"""
    def __init__(self, path:str = "/home/solver/dataset", dataset_type:str = "train") -> None:
        super().__init__()
        filename = f"fewrel_{dataset_type}.json"
        with open(join(path,filename), 'r', encoding="utf-8") as f:
            relations = json.load(f)
        X = list(chain(*relations.values()))
        self.tokens, self.e1, self.e2 = list(zip(*[[x['tokens'], x['h'], x['t']] for x in X]))
        self.labels = list(chain(*[[k]*len(relations[k]) for k,v in relations.items()]))
    def __len__(self) -> int:
        return len(self.labels)
    def __getitem__(self, idx):
        return self.tokens[idx], self.e1[idx], self.e2[idx], self.labels[idx]

class TokensDataset(Dataset):
    def __init__(self, tokens_path, label_dict_path, label_desc_path, tokenizer) -> None:
        super().__init__()
        with open(tokens_path, 'r', encoding="utf-8") as f:
            relations = json.load(f)
        with open(label_dict_path, 'r', encoding="utf-8") as f:
            id2pid = json.load(f)
        with open(label_desc_path, 'r', encoding="utf-8") as f:
            pid2desc = json.load(f)
        pid2id = {v:k for k,v in id2pid.items()}
        X = list(chain(*relations.values()))
        token_ids = [x['tokens'] for x in X]
        tokens = tokenizer(token_ids, return_tensors='pt', truncation=True, padding='max_length', max_length=512, is_split_into_words=True)
        self.input_ids, self.attention_mask = tokens['input_ids'], tokens['attention_mask']
        labels = list(chain(*[[k]*len(relations[k]) for k,v in relations.items()]))
        self.labels = [int(pid2id[l]) if l in pid2id.keys() else -1 for l in labels]
        descriptions = [' : '.join(pid2desc[l]) for l in labels]
        tokens = tokenizer(descriptions, return_tensors='pt', truncation=True, padding='max_length', max_length=512)
        self.desc_input_ids, self.desc_attention_mask = tokens['input_ids'], tokens['attention_mask']
    def __len__(self) -> int:
        return len(self.labels)
    def __getitem__(self, idx):
        return self.input_ids[idx], self.attention_mask[idx], self.desc_input_ids[idx], self.desc_attention_mask[idx], self.labels[idx]