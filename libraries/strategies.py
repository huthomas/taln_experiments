import pickle, json 

import nltk 
import numpy as np
import pandas as pd  
import operator as op 
import itertools as it, functools as ft 

import networkx as nx

import torch as th 
import torch.nn as nn 
import torch.nn.functional as F 
from torch.utils.data import Dataset

from os import path 
from os.path import join
from glob import glob
from time import time 
from math import ceil 
import re
import html
import random
from rich.progress import track

import typing
from typing import List, Tuple

from nltk.cluster import KMeansClusterer
import spacy
from sentence_transformers import SentenceTransformer, util
from transformers import AutoTokenizer, RobertaTokenizer, AutoModel, RobertaConfig, RobertaAdapterModel

from sklearn.metrics import f1_score
from scipy.spatial.distance import mahalanobis

from libraries.log import logger 

neural_network_config = {
    'layers_config': [], 
    'activations': [], 
    'drop_val': 0.1
}

map_serializer2mode = {
    json: ('r', 'w'), 
    pickle: ('rb', 'wb')
}


def measure(func):
    @ft.wraps(func)
    def _measure(*args, **kwargs):
        start = int(round(time() * 1000))
        try:
            return func(*args, **kwargs)
        finally:
            end_ = int(round(time() * 1000)) - start
            duration = end_ if end_ > 0 else 0
            logger.debug(f"{func.__name__:<20} total execution time: {duration:04d} ms")
    return _measure

def serialize(path2location, data, serializer=pickle):
    mode = map_serializer2mode.get(serializer, None)
    if mode is None:
        raise ValueError(f'serializer option must be pickle or json')
    with open(path2location, mode=mode[1]) as fp:
        serializer.dump(data, fp)

def deserialize(path2location, serializer=pickle):
    mode = map_serializer2mode.get(serializer, None)
    if mode is None:
        raise ValueError(f'serializer option must be pickle or json')
    with open(path2location, mode=mode[0]) as fp:
        data = serializer.load(fp)
        return data

def to_sentences(document):
    sentences = nltk.sent_tokenize(document)
    sentences = [ sent.strip().lower() for sent in sentences ]
    return sentences

def to_chunks(sentences, chunk_size=7):
    chunks = []
    nb_sentences = len(sentences)
    for idx in range(0, nb_sentences, chunk_size):
        chunks.append('\n'.join(sentences[idx:idx+chunk_size]))
    return chunks

def vectorize(data, vectorizer, device='cpu', to_tensor=True):
    with th.no_grad():
        output = vectorizer.encode(data, device=device, convert_to_tensor=to_tensor, show_progress_bar=True)
    return output

def vectorize_from_model(dataloader, model, tokenizer, device, entities=False):
    """Returns the pooled logits of the input data through the model"""
    emb = []
    if entities:
        with th.no_grad():
            for relation in track(iter(dataloader),"Embedding batches of sentences..."):
                names = [list(n) for n in zip(*relation['entity_names'])]
                spans = [list(t) for t in zip(list(zip(*relation['entity_spans'][0])), list(zip(*relation['entity_spans'][1])))]
                tokens = tokenizer(relation['text'], padding=True, max_length=512, truncation=True, return_tensors='pt', entity_spans=spans, entities=names).to(device)
                emb.append(model(**tokens)['pooler_output'])
    else:
        with th.no_grad():
            for relation in track(iter(dataloader),"Embedding batches of sentences..."):
                tokens = tokenizer(relation['text'], padding=True, max_length=512, truncation=True, return_tensors='pt').to(device)
                emb.append(model(**tokens)['pooler_output'])
    return th.concat(emb, dim=0)

def load_vectorizer(path2vectorizer):
    if path.isfile(path2vectorizer):
        vectorizer = deserialize(path2vectorizer)
        logger.success('the vectorizer was loaded')
    else:
        try:
            _, model_name = path.split(path2vectorizer)
            vectorizer = SentenceTransformer(model_name)
            serialize(path2vectorizer, vectorizer)
            logger.success(f'the vectorizer was downloaded and saved at {path2vectorizer}')
        except Exception as e:
            logger.error(e)
            raise ValueError(f'{path2vectorizer} is not a valid path')
    return vectorizer

def load_bulk(path2bulk, clean=False):
    with open(path2bulk) as f:
        data=[]
        for line in f.readlines():
            data.append(json.loads(line))
    df = pd.json_normalize(data)
    if clean:
        df['text'] = df['text'].apply(clean_str)
    return df

def clean_str(string):
    string = re.sub('<.+?>', '', string)
    return html.unescape(string)

def extract_entity_pos(sentences: List[str], print_entities: bool = False, model:str = "fr_core_news_md", tokenize: bool = False) -> List[Tuple[Tuple[int, int], Tuple[int, int]]]:
    """Return the start and end positions of entities in a list of sentences"""
    nlp = spacy.load(model)
    pos = []
    toks = []
    for doc in nlp.pipe(sentences, disable=["tok2vec", "tagger", "parser", "attribute_ruler", "lemmatizer"]):
        ents = doc.ents
        pos += [[(ent.start, ent.end) for ent in ents]]
        if print_entities: print(ents)
        if tokenize: toks.append([str(t) for t in list(doc)])
    if tokenize:
        return pos, toks
    return pos

def add_entity_markers(sentences: List[str], markers: List[str] = ['<e1>','</e1>','<e2>', '</e2>']) -> List[str]:
    """Add entity markers around a two-entity sentence"""
    marked_sentences = []
    pos, sentences = extract_entity_pos(sentences, tokenize=True)
    sentences, pos = list(zip(*[(s,p) for s,p in zip(sentences, pos) if len(p)==2]))
    for s, ((s1, e1), (s2, e2)) in zip(sentences, pos):
        offset = 0
        for i, m in enumerate([s1,e1,s2,e2]):
            s.insert(m+offset, markers[i])
            offset+=1
        marked_sentences.append(' '.join(s))
    return marked_sentences

def cut_affixes(sentences:List[str], as_dict:bool = False) -> List[str]:
    """Cut a sentence in prefix/e1/infix/e2/suffix format according to its entities (only works for two-entities sentences)"""
    cut_sentences = []
    pos, sentences = extract_entity_pos(sentences, tokenize=True)
    sentences, pos = list(zip(*[(s,p) for s,p in zip(sentences, pos) if len(p)==2]))
    for i, s in enumerate(sentences):
        (e1, s1), (e2, s2) = pos[i]
        if as_dict:
            cut_sentences.append({"prefix":' '.join(s[:e1]),"e1": ' '.join(s[e1:s1]),"infix": ' '.join(s[s1:e2]), "e2": ' '.join(s[e2:s2]), "suffix": ' '.join(s[s2:])})
        else:
            cut_sentences.append([' '.join(s[:e1]), ' '.join(s[e1:s1]), ' '.join(s[s1:e2]), ' '.join(s[e2:s2]), ' '.join(s[s2:])])
    return cut_sentences

def compute_metrics(eval_pred):
    """A method to compute the F1-score of a model output on gold labels"""
    logits, labels = eval_pred
    predictions = np.argmax(logits, axis=-1)
    return {'microf1':f1_score(labels, predictions, average='micro'), 'macrof1': f1_score(labels,predictions, average='macro')}

def get_fewrel_datasetdict(filepath:str, val_split:float = 0.0, max_relation_examples=0, return_label_names=False, keep_entity_span=False) -> None:
    """A method to load FewRel relationships for model training and testing"""
    with open(path.join(filepath,"fewrel_train.json"), 'r', encoding="utf-8") as f:
        train = json.load(f)
    with open(path.join(filepath,"fewrel_val.json"), 'r', encoding="utf-8") as f:
        test = json.load(f)

    pids = list(set([k for k,_ in train.items()] + [k for k,_ in test.items()]))
    with open(path.join(filepath,'pid2name.json'), 'r', encoding='utf-8') as f:
        pid2name = json.load(f)
    pid2name = {k:v for k,v in pid2name.items() if k in pids}
    pid2id = {k:i for i,(k,v) in enumerate(pid2name.items())}
    label_names = list(pid2name.keys())

    if max_relation_examples>0:
        train={k:v[:max_relation_examples] for k,v in train.items()}
        test={k:v[:max_relation_examples] for k,v in test.items()}
    Xtrain, Xtest = list(it.chain(*train.values())), list(it.chain(*test.values()))
    train_tokens, test_tokens = [x['tokens'] for x in Xtrain], [x['tokens'] for x in Xtest]
    train_ents, test_ents = [(x['h'][-1][0], x['t'][-1][0]) for x in Xtrain],[(x['h'][-1][0], x['t'][-1][0]) for x in Xtest]
    train_labels = list(it.chain(*[[pid2id[k]]*len(train[k]) for k,v in train.items()]))
    test_labels = list(it.chain(*[[pid2id[k]]*len(test[k]) for k,v in test.items()]))
    if(val_split):
        SPLIT = int(len(train_tokens)*(1-val_split))
        temp = list(zip(train_tokens, train_ents, train_labels))
        random.shuffle(temp)
        train_tokens, train_ents, train_labels = zip(*temp)
        train_tokens, train_ents, train_labels = list(train_tokens), list(train_ents), list(train_labels)
        train_tokens, val_tokens = train_tokens[:SPLIT], train_tokens[SPLIT:]
        train_ents, val_ents = train_ents[:SPLIT], train_ents[SPLIT:]
        train_labels, val_labels = train_labels[:SPLIT], train_labels[SPLIT:]
        datasetdict = DatasetDict({'train': Dataset.from_dict({'text':train_tokens, 'entities':train_ents, 'label':train_labels}),\
            'test':Dataset.from_dict({'text':test_tokens, 'entities':test_ents, 'label':test_labels}),\
            'val':Dataset.from_dict({'text':val_tokens, 'entities':val_ents, 'label':val_labels})})
        if return_label_names:
            return datasetdict, label_names
        else:
            return datasetdict
    if keep_entity_span:
        datasetdict = DatasetDict({'train': Dataset.from_dict({'text':train_tokens, 'entities':train_ents, 'label':train_labels}),\
            'test':Dataset.from_dict({'text':test_tokens, 'entities':test_ents, 'label':test_labels})})
    else:
        datasetdict = DatasetDict({'train': Dataset.from_dict({'text':train_tokens, 'label':train_labels}),\
        'test':Dataset.from_dict({'text':test_tokens,'label':test_labels})})
    if return_label_names:
        return datasetdict,label_names
    else:
        return datasetdict

def tokenize_function(tokenizer, examples):
    """Encode input tokens to their id"""
    return tokenizer(examples["text"], padding="max_length", truncation=True, is_split_into_words=True)

def print_similarity(embeddings, relations, same_pid=False):
    k1, k2 = random.choice(list(embeddings.keys())), random.choice(list(embeddings.keys()))
    if same_pid:
        k1=k2
    id1, id2 = random.randint(0,len(embeddings[k1])), random.randint(0,len(embeddings[k2]))
    emb1, emb2 = embeddings[k1][id1,:], embeddings[k2][id2,:]
    sim = emb1 @ emb2.T
    sent1, sent2 = ' '.join(relations[k1][id1]['tokens']), ' '.join(relations[k2][id2]['tokens'])
    print(f"Relation 1 : {sent1} | pid {k1}")
    print(f"Relation 2 : {sent2} | pid {k2}")
    print(f"Similarity : {sim}")


def nb_characters_in_split_words(words, spans):
    return sum([len(words[i]) for i in spans]) + len(spans)

def word_to_character_based_spans(sentence, entity_spans):
    begin = nb_characters_in_split_words(sentence, range(entity_spans[0][0]))
    spans1 = [begin, begin+nb_characters_in_split_words(sentence, entity_spans[0])-1]
    begin = nb_characters_in_split_words(sentence, range(entity_spans[-1][0]))
    spans2 = [begin, begin+nb_characters_in_split_words(sentence, entity_spans[-1])-1]
    return ' '.join(sentence), (spans1,spans2)

def get_indices(n):
    pairs = th.tensor(list(it.product(range(n), range(n))))
    r = list(range(n*n))
    size = range(n)
    indices = list(size)
    for s in size:
        s += 1
        indices += r[n*s+s:n*(s+1)]
    return indices, [t.squeeze(-1) for t in pairs[indices,:].split(1,dim=1)]

def compute_shortest_dependency_path(example: dict)->(list, tuple, tuple):
    """Compute the sortest dependency path between the two entities of a relation"""
    switch=False #Whether the object is locate before the object
    if example['subj_start']>example['obj_end']:
        tmp = example['subj_start'], example['subj_end']
        example['subj_start'], example['subj_end']=example['obj_start'], example['obj_end']
        example['obj_start'], example['obj_end'] = tmp
        switch=True
    edges = [(i,h-1) for i,h in enumerate(example['stanford_head']) if h>0]
    #print(f"edges {edges} {(example['subj_start'], example['subj_end'],example['obj_start'], example['obj_end'])} {example['token']}")
    g = nx.Graph(edges) # Create a graph from syntaxic dependencies
    source_target = list(it.product(list(range(example['subj_start'], example['subj_end']+1)),list(range(example['obj_start'], example['obj_end']+1))))
    has_path = [st for st in source_target if nx.has_path(g, st[0], st[1])] # Whether a path exists between the two entity nodes

    if has_path: # Compute the shortest existing path
        
        shortest_path = nx.shortest_path(g, source=has_path[0][0], target=has_path[0][1])[1:-1]
        
        shortest_path = list(range(example['subj_start'], example['subj_end']+1))+shortest_path+list(range(example['obj_start'], example['obj_end']+1))
    else: # If none exist, take all tokens bewteen the two entities
        shortest_path =list(range(example['subj_start'], example['obj_end']+1))
    
    if switch:
        shortest_path = list(shortest_path)
        shortest_path.reverse()
        obj_pos=(0, example['obj_end']-example['obj_start'])
        subj_pos=(len(shortest_path)-(example['subj_end']-example['subj_start'])-1,len(shortest_path)-1)
    else:
        subj_pos=(0, example['subj_end']-example['subj_start'])
        obj_pos=(len(shortest_path)-(example['obj_end']-example['obj_start'])-1,len(shortest_path)-1)
    #print(example['token'], shortest_path, subj_pos, obj_pos)
    return [example['token'][i] for i in shortest_path], subj_pos, obj_pos

class EntityDataset(Dataset):
    """A class to hold a sentence dataset for entity type prediction"""
    def __init__(self, filepath:str, savepath:str="", ent_representation="ent", shortest_path=False, keep_no_rel=False, dataset="TACRED", shuffle_words=False)->None:
        super(Dataset, self).__init__()
        if not ent_representation in ['ent', 'ent_type', 'ent_markup']:
            raise Exception("Invalid entity_representation")
        self.ent_start = "<ENT>"
        self.ent_end = "</ENT>"
        
        if path.exists(savepath):
            with open(savepath,'rb') as f:
                idx, sentence, ent1, ent2, relation, entity_spans = pickle.load(f)
            self.idx, self.sentence, self.ent1, self.ent2, self.relation, self.entity_spans = idx, sentence, ent1, ent2, relation, entity_spans

        else:
            self.idx, self.sentence, self.ent1, self.ent2, self.relation, self.entity_spans = [], [], [], [], [], []
            with open(filepath, 'r', encoding='utf-8') as f:
                data = json.load(f)
            data = sorted(data, key=lambda x:x['id'])
            examples = []
            for i, item in enumerate(track(data, "computing dataset examples...")):
                if item['relation']!='no_relation' or keep_no_rel:
                    if shortest_path:
                        item["token"], (item["subj_start"], item["subj_end"]), (item["obj_start"], item["obj_end"]) = \
                        compute_shortest_dependency_path(item)
                    tokens = item["token"]
                    token_spans = dict(
                        subj=(item["subj_start"], item["subj_end"] + 1),
                        obj=(item["obj_start"], item["obj_end"] + 1)
                    )

                    if token_spans["subj"][0] < token_spans["obj"][0]:
                        entity_order = ("subj", "obj")
                    else:
                        entity_order = ("obj", "subj")

                    text = ""
                    cur = 0
                    char_spans = {}
                    for target_entity in entity_order:
                        token_span = token_spans[target_entity]
                        text += " ".join(tokens[cur : token_span[0]])
                        if text:
                            text += " "
                        char_start = len(text)
                        if ent_representation=='ent':
                            text += " ".join(tokens[token_span[0] : token_span[1]])
                            char_end = len(text)
                            char_spans[target_entity] = (char_start, char_end)
                            text += " "
                        elif ent_representation=='ent_type':
                            text += item[target_entity+"_type"]
                            char_end = len(text)
                            char_spans[target_entity] = (char_start, char_end)
                            text += " "
                        else:
                            text += self.ent_start
                            char_end = len(text)
                            char_spans[target_entity] = (char_start, char_end)
                            text += " ".join(tokens[token_span[0] : token_span[1]])+self.ent_end+" "
                        cur = token_span[1]
                    text += " ".join(tokens[cur:])
                    text = text.rstrip()
                    self.idx.append(i)
                    if shuffle_words:
                        text = text.split(' ')
                        random.shuffle(text)
                        text = " ".join(text)
                    self.sentence.append(text)
                    self.ent1.append(item['subj_type'])
                    self.ent2.append(item['obj_type'])
                    self.relation.append(item['relation'])
                    self.entity_spans.append([tuple(char_spans["subj"]), tuple(char_spans["obj"])])
            if savepath:
                with open(savepath,'wb') as f:
                    pickle.dump([self.idx, self.sentence, self.ent1, self.ent2, self.relation, self.entity_spans], f)
        if dataset=="TACRED":
            self.label2id = {rel:i for i,rel in enumerate(sorted(list(set(self.relation))))}
        else:
            with open("/udd/huthomas/taln_experiments/dataset/id2pid.json", 'r') as f:
                id2pid = json.load(f)
            self.label2id = {v:int(k) for k,v in id2pid.items()}
        self.ent2id = {ent:i for i,ent in enumerate(sorted(list(set(self.ent1+self.ent2))))}
    def __len__(self)->int:
        return len(self.idx)
    def __getitem__(self, i):
        return self.idx[i], self.sentence[i], self.ent1[i], self.ent2[i], self.relation[i], self.entity_spans[i]

def build_model(model_name, adapters=[], classification_heads=[], num_labels=-1, headid2label=[]):
    tokenizer = AutoTokenizer.from_pretrained(model_name, add_prefix_space=True)
    if adapters:
        config = RobertaConfig.from_pretrained(
            model_name,
            num_labels=num_labels,
        )
        encoder = RobertaAdapterModel.from_pretrained(
            model_name,
            config=config,
        )
        for adapter in adapters:
            encoder.add_adapter(adapter)
        if classification_heads:
            headnumlabels = [len(iddict) for iddict in headid2label]
            for head, nb, iddict in zip(classification_heads, headnumlabels, headid2label):
                encoder.add_classification_head(
                    head,
                    num_labels=nb,
                    id2label=iddict
                )
        encoder.train_adapter(adapters)
    else:
        encoder = AutoModel.from_pretrained(model_name)
    return tokenizer, encoder

def collate_fn(data: list) ->tuple:
    """Collate function for dataloaders, to preserve the entity_spans structure"""
    idx, sentence, ent1, ent2, relation, spans = zip(*data)
    return th.tensor(idx).long(), list(sentence), list(ent1), list(ent2), list(relation), list(spans)

def to_one_hot(vec, dim):
    out = np.zeros((vec.size, dim))
    out[np.arange(vec.size), vec] = 1
    return out

def get_vector(entity, keyedvectors, index_to_key):
    if "ENTITY/"+entity in index_to_key:
        return keyedvectors.get_vector("ENTITY/"+entity)
    elif entity.lower() in index_to_key:
        return keyedvectors.get_vector(entity.lower())
    else:
        return np.random.randn(keyedvectors.vector_size)
    
def jaccard_similarity(x,y):
    """ returns the jaccard similarity between two lists """
    x = x.get('lemma')#[d.lemma_ for d in nlp(x)]
    y = y.get('lemma')#[d.lemma_ for d in nlp(y)]
    intersection_cardinality = len(set.intersection(*[set(x), set(y)]))
    union_cardinality = len(set.union(*[set(x), set(y)]))
    return intersection_cardinality/float(union_cardinality)

def sentence_transformer_similarity(x,y, sent_transformer):
    embeddings1 = sent_transformer.encode(x, convert_to_tensor=True)
    embeddings2 = sent_transformer.encode(y, convert_to_tensor=True)
    return util.cos_sim(embeddings1,embeddings2).squeeze(-1).item()

def build_children(tree):
    out = []
    for i, w in enumerate(tree):
        acc = []
        for j,child in enumerate(tree):
            if child[0]==i+1:
                acc.append([w[0], w[1], j+1])
        if not acc:
            acc.append(w)
        out+=acc
    return out

def s(a,b):
    return a==b

def q(a,b):
    THETA = 2.0
    return int(a==b)*THETA + int(a!=b)

def sim(a,b):
    da, ha, ta = a
    db, hb, tb = b
    return (s(da, db)+s(ha, hb))*q(ta, tb)

def SABK(a,b):
    #atext = a.get('lemma')
    atext = a.get('text')
    a = a.get(['head', 'deprel'])
    a = build_children(a)
    a = [[atext[elem[0]-1], elem[1], atext[elem[2]-1] if len(elem)==3 else ''] for elem in a]
    
    #btext = b.get('lemma')
    btext = b.get('text')
    b = b.get(['head', 'deprel'])
    b = build_children(b)
    b = [[btext[elem[0]-1], elem[1], btext[elem[2]-1] if len(elem)==3 else ''] for elem in b]
    
    similarity=0.0
    for aword in a:
        for bword in b:
            similarity+=sim(aword, bword)
    similarity/=(len(atext)+len(btext))
    return similarity

class Mahalanobis:
    def __init__(self, vectors):
        self.VI = np.linalg.pinv(np.cov(vectors.T))
    def dist(self, u,v):
        return mahalanobis(u, v, self.VI)