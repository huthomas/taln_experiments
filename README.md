# Experiments for TALN paper "Derrière les plongements de relations", H. Thomas 2023



## Train a model
The scripts rely on environment variables : you can for instance give them before the python command or set them beforehand in bash.

```
# Information about the function and parameters
CHECKPOINTS=/path/to/checkpoints DATASET=/path/to/dataset FEATURES=/path/to/features python3 main.py train --help

# Run training on roberta model without entities
CHECKPOINTS=/path/to/checkpoints DATASET=/path/to/dataset FEATURES=/path/to/features python3 main.py train --train_tokens trainfile.json --val_tokens valfile.json --test_tokens testfile.json --embedding_model roberta-base --model_type without_ents --shortest_path False --ent_representation ent --nb_epochs 32 --batch_size 8
```

## Analyze embeddings (lexically, syntaxically, semantically)


```
# Information about the function and parameters
MODELS=/path/to/models DATASET=/path/to/dataset FEATURES=/path/to/features python3 main.py train --help

MODELS=/path/to/models DATASET=/path/to/dataset FEATURES=/path/to/features python3 main.py train --embeddings_filename embeddings.pkl --batch_size 100 --sim_threshold 10.0
```

## Link to the paper

*coming soon*